
#ifndef SRUN_LIB_PROTOCOL_H
#define SRUN_LIB_PROTOCOL_H 1

/**
 * SRun User Protocol
 */

#include <stdint.h>

#include <uuid.h>

#define SRUP_MAGIC   0x53525550 // ASCII 'SRUP'
#define SRUP_VERSION 1

#define SRUP_REQ_PAYLOAD_TIMEOUT 200 // 200ms

#include "common.h"

// in host endian
struct srup_header {
	uint32_t magic;
	uint8_t version;
	uint8_t type;
	uint16_t next_len;
	union {
		uint64_t r1;
		uint8_t fd_num;    // for req_new
		uint16_t err_code; // for rsp_err
	};
	uuid_t uuid;
};
_Static_assert(sizeof(struct srup_header) % 8 == 0, "Sizeof struct srup_header is not aligned");
typedef struct srup_header srup_header_t;

enum {
	SRUP_ERR_INVALID,
	SRUP_ERR_DENIED,
	SRUP_ERR_NOTFOUND,
	SRUP_ERR_INTERNAL,
};
extern const char *srup_errmsg(int errcode);

enum {
	/*
	 * Simple continue rsp, for req that has payload
	 *  No payload
	 *  UUID should match the one in req, which may be empty
	 */
	SRUP_TYPE_RSP_CONTINUE = 0,
	/*
	 * Req to run a new command
	 *  Payload:
	 *   srun_msg (msgpack format) in `next_len` length,
	 *   with optional `fd_num` of fd as ancillary data
	 *  UUID should be zeroed
	 */
	SRUP_TYPE_REQ_NEW,
	/*
	 * Simple OK rsp
	 *  No Payload
	 *  UUID will be set to the new one for new req, or the same one with req
	 */
	SRUP_TYPE_RSP_OK,
	/*
	 * Simple ERR rsp
	 *  No Payload
	 *  UUID:
	 *   should match if req is not new_req
	 *   may be empty or a new one for req
	 */
	SRUP_TYPE_RSP_ERR,
	/*
	 * Req to update command exec info
	 *  Payload:
	 *   TODO
	 *  UUID should be set
	 */
	SRUP_TYPE_REQ_UPDATE,
	/*
	 * Req to query command exec info
	 *  Payload:
	 *   TODO
	 *  UUID should be set
	 */
	SRUP_TYPE_REQ_QUERY,
	/*
	 * Req to terminate previous command
	 *  Payload:
	 *   TODO
	 *  UUID should be set
	 */
	SRUP_TYPE_REQ_TERMINATE,
};

/*
 * Helper Functions
 */

#include <nebase/cdefs.h>

#define SRUND_CONNECT_TIMEOUT 1000
#define SRUND_WAIT_TIMEOUT 200

extern const char *srund_addr;

extern int srun_proto_new(const char *msg, int len, int *fdp, int fd_num, uuid_t uuid)
	_nattr_warn_unused_result _nattr_nonnull((1, 5));

#endif
