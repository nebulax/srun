
#ifndef SRUN_LIB_MESSAGE_H
#define SRUN_LIB_MESSAGE_H 1

#include <nebase/cdefs.h>

#include "common.h"

enum {
	SRUN_MSG_ROOT_KEY_NONE = 0,
	SRUN_MSG_ROOT_KEY_FILENAME,      // string
	SRUN_MSG_ROOT_KEY_ARGV,          // string array
	SRUN_MSG_ROOT_KEY_ENVP,          // string array
	SRUN_MSG_ROOT_KEY_ORPHAN,        // boolean
	SRUN_MSG_ROOT_KEY_IO,            // io type string array
	SRUN_MSG_ROOT_KEY_WAIT,          // wait map
	SRUN_MSG_ROOT_KEY_CONCURRENCY,   // concurrency map
	SRUN_MSG_ROOT_KEY_CRED,          // cred map
	SRUN_MSG_ROOT_KEY_SCHED,         // sched map
	SRUN_MSG_ROOT_KEY_RLIMIT,        // string:string map TODO
	SRUN_MSG_ROOT_KEY_CAPABILITIES,  // string array, with "CAP_" removed, TODO
	SRUN_MSG_ROOT_KEY_PRIVILEGES,    // string array, with "PRIV_" removed, TODO
	SRUN_MSG_ROOT_KEY_SECCOMP,       // string array TODO
	SRUN_MSG_ROOT_KEY_CAPSICUM,      // TODO
	SRUN_MSG_ROOT_KEY_CGROUP,        // map TODO
	SRUN_MSG_ROOT_KEY_NAMESPACE,     // map TODO
};

enum {
	SRUN_MSG_IO_TYPE_NONE = 0,
	SRUN_MSG_IO_TYPE_PASS,
	SRUN_MSG_IO_TYPE_KEEP,
	SRUN_MSG_IO_TYPE_PTY,
	SRUN_MSG_IO_TYPE_STDIN,
	SRUN_MSG_IO_TYPE_STDOUT,
	SRUN_MSG_IO_TYPE_STDERR,
	SRUN_MSG_IO_TYPE_DFT,         // Use default io file
};
extern const char *io_type_str(int io_type);
extern int io_type_is_conflict(int ntype, int otype);

enum {
	SRUN_MSG_WAIT_KEY_TIMEOUT = 0,   // int in msec, default to 10000 if not set
	// only one of the following should be set
	SRUN_MSG_WAIT_KEY_SIGNAL,        // int signum
	SRUN_MSG_WAIT_KEY_FDIN,          // int fd index in io array, hup will be considered as error
	SRUN_MSG_WAIT_KEY_FDHUP,         // int fd index in io array
};

enum {
	SRUN_MSG_CONCURRENCY_KEY_ID = 0, // uuid
	SRUN_MSG_CONCURRENCY_KEY_NUM,    // int
	SRUN_MSG_CONCURRENCY_KEY_TIMEOUT,// int in msec, <0 for wait forever, =0 for skip
};

enum {
	SRUN_MSG_CRED_KEY_NONE = 0,
	SRUN_MSG_CRED_KEY_UID,
	SRUN_MSG_CRED_KEY_GID,
};

enum {
	SRUN_MSG_SCHED_KEY_NONE = 0,
	SRUN_MSG_SCHED_KEY_POLICY,
	SRUN_MSG_SCHED_KEY_PRIORITY,
};

/*
 * Helper Functions
 */

#include <msgpack.h>
#include <jansson.h>

extern int srun_msg_pack_from_json(msgpack_packer* mpk, json_t *jobj)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

#endif
