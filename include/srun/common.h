
#ifndef SRUN_LIB_TYPES_H
#define SRUN_LIB_TYPES_H 1

#define SRUN_MSG_MAX_FDS 8

#define SRUN_SIGNO_MIN 1
#define SRUN_SIGNO_MAX 31

#endif
