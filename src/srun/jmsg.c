
#include <nebase/syslog.h>
#include <srun/message.h>
#include <srun/protocol.h>

#include "jmsg.h"

#include <stdio.h>
#include <unistd.h>

#include <msgpack.h>
#include <uuid.h>

int jmsg_init(void)
{
	if (jmsg_env_init() != 0)
		return -1;
	if (jmsg_io_init() != 0)
		return -1;
	if (jmsg_conc_init() != 0)
		return -1;
	return 0;
}

void jmsg_deinit(void)
{
	jmsg_env_deinit();
	jmsg_io_deinit();
	jmsg_conc_deinit();
}

json_t *jmsg_new(int argc, char *argv[])
{
	json_t *exec_msg = json_object();
	if (!exec_msg) {
		neb_syslog(LOG_ERR, "Failed to get new exec_msg");
		return NULL;
	}

	json_t *cmd_obj = json_string(argv[0]);
	if (json_object_set_new(exec_msg, "filename", cmd_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add filename to root obj");
		json_decref(exec_msg);
		return NULL;
	}
	if (argc == 1)
		return exec_msg;

	json_t *argv_obj = json_array();
	if (json_object_set_new(exec_msg, "argv", argv_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add argv to root obj");
		json_decref(exec_msg);
		return NULL;
	}

	for (int i = 1; i < argc; i++) {
		json_t *obj = json_string(argv[i]);
		if (json_array_append_new(argv_obj, obj) != 0) {
			neb_syslog(LOG_ERR, "Failed to add %s to argv", json_string_value(obj));
			json_decref(obj);
			json_decref(exec_msg);
			return NULL;
		}
	}

	return exec_msg;
}

int jmsg_finalize(json_t *exec_msg, int fds[SRUN_MSG_MAX_FDS], int *fd_num)
{
	*fd_num = 0;
	if (jmsg_env_set(exec_msg) != 0)
		return -1;
	if (jmsg_io_set(exec_msg, fds, fd_num) != 0) // caller should close fds
		return -1;
	if (jmsg_conc_set(exec_msg) != 0)
		return -1;
	return 0;
}

void jmsg_del(json_t *exec_msg, int *fds, int fd_num)
{
	if (fds) {
		for (int i = 0; i < fd_num; i++) {
			int fd = fds[i];
			if (fd >= 0)
				close(fds[i]);
			fds[i] = -1;
		}
	}
	json_decref(exec_msg);
}

void jmsg_dump(FILE *stream, json_t *exec_msg)
{
	json_dumpf(exec_msg, stream, JSON_INDENT(2) | JSON_SORT_KEYS);
}

int jmsg_send(json_t *exec_msg, int *fds, int fd_num)
{
	int ret = 0;
	msgpack_sbuffer sbuf;
	msgpack_sbuffer_init(&sbuf);
	msgpack_packer pk;
	msgpack_packer_init(&pk, &sbuf, msgpack_sbuffer_write);

	if (srun_msg_pack_from_json(&pk, exec_msg) != 0) {
		fprintf(stderr, "Failed to get packed msg\n");
		ret = -1;
		goto exit_free_sbuf;
	}

	uuid_t uuid;
	if (srun_proto_new(sbuf.data, sbuf.size, fds, fd_num, uuid) != 0) {
		fprintf(stderr, "Failed to run new command\n");
		ret = -1;
		goto exit_free_sbuf;
	}

	char out[UUID_STR_LEN];
	uuid_unparse_upper(uuid, out);
	out[UUID_STR_LEN - 1] = '\0';
	fprintf(stdout, "UUID: %s\n", out);

exit_free_sbuf:
	msgpack_sbuffer_destroy(&sbuf);
	return ret;
}
