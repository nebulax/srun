
#include <nebase/syslog.h>

#include <srun/message.h>

#include <stdio.h>

#include <jansson.h>
#include <msgpack.h>

static int msgpack_dump(json_t *robj)
{
	msgpack_sbuffer sbuf;
	msgpack_sbuffer_init(&sbuf);
	msgpack_packer pk;
	msgpack_packer_init(&pk, &sbuf, msgpack_sbuffer_write);

	if (srun_msg_pack_from_json(&pk, robj) != 0) {
		fprintf(stderr, "Failed to get packed msg\n");
		return -1;
	}

	msgpack_unpacked result;
	msgpack_unpack_return ret = MSGPACK_UNPACK_SUCCESS;
	msgpack_unpacked_init(&result);

	size_t off = 0;
	ret = msgpack_unpack_next(&result, sbuf.data, sbuf.size, &off);
	if (ret != MSGPACK_UNPACK_SUCCESS) {
		fprintf(stderr, "Invalid message: msgpack_unpack_next: %d", ret);
		return -1;
	} else if (off != sbuf.size) {
		fprintf(stderr, "There is %lu more data left in this message", off);
		return -1;
	}

	msgpack_object_print(stdout, result.data);
	return 0;
}

int main(int argc, char *argv[])
{
	neb_syslog_init(NEB_LOG_STDIO, argv[0]);

	json_error_t jerr;
	json_t *obj = NULL;
	size_t flag = JSON_ALLOW_NUL;
	if (argc > 1)
		obj = json_load_file(argv[1], flag, &jerr);
	else
		obj = json_loadf(stdin, flag, &jerr);
	if (!obj) {
		fprintf(stderr, "%s[%d:%d]: %s\n", jerr.source, jerr.line, jerr.column, jerr.text);
		return -1;
	}

	return msgpack_dump(obj);
}
