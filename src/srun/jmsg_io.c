
#include <nebase/syslog.h>

#include "jmsg.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>

#include <glib.h>

/**
 * \note fd will be stealed after jmsg_io_set, in which index will be set to >=0
 */
struct io_map_v {
	char *orig_value;
	const char *type;
	int fd;
	int index;
};

static GHashTable *io_map = NULL;
static int use_default_io = 0;

const char io_type_stdout[] = "stdout";
const char io_type_stderr[] = "stderr";
const char io_type_stdin[] = "stdin";
const char io_type_pass[] = "pass";
const char io_type_keep[] = "keep";
const char io_type_pty[] = "pty";
const char io_type_dft[] = "dft";

static void io_map_v_free(gpointer data)
{
	struct io_map_v *v = data;
	if (v->fd >= 0)
		close(v->fd);
	if (v->orig_value)
		free(v->orig_value);
	free(v);
}

static int cmd_get_fd(const char *value, int flags)
{
	if (flags & O_RDWR) {
		neb_syslog(LOG_ERR, "No RDWR io support");
		return -1;
	}
	if (value[0] == '@') {
		value += 1;
		int internal_fd = -1;
		if (strcmp(value, "stdout") == 0) {
			if (flags & O_RDONLY) {
				neb_syslog(LOG_ERR, "stdout doesn't support read");
				return -1;
			}
			internal_fd = fileno(stdout);
		} else if (strcmp(value, "stderr") == 0) {
			if (flags & O_RDONLY) {
				neb_syslog(LOG_ERR, "stderr doesn't support read");
				return -1;
			}
			internal_fd = fileno(stderr);
		} else if (strcmp(value, "stdin") == 0) {
			if (flags & O_WRONLY) {
				neb_syslog(LOG_ERR, "stdin doesn't support write");
				return -1;
			}
			internal_fd = fileno(stdin);
		}

		int new_fd = dup(internal_fd);
		if (new_fd == -1)
			neb_syslog(LOG_ERR, "dup(%s): %m", value);
		return new_fd;
	} else {
		int fd = open(value, flags, 0640);
		if (fd == -1)
			neb_syslog(LOG_ERR, "open(%s): %m", value);
		return fd;
	}
}

static int io_map_replace(const char *key, const char *value, const char *type, int fd)
{
	if (!io_map) {
		neb_syslog(LOG_ERR, "io_map is empty");
		return -1;
	}
	char *k = strdup(key);
	if (!k) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return -1;
	}
	struct io_map_v *v = malloc(sizeof(struct io_map_v));
	if (!v) {
		neb_syslog(LOG_ERR, "malloc: %m");
		free(k);
		return -1;
	}
	v->orig_value = strdup(value);
	if (!v->orig_value) {
		neb_syslog(LOG_ERR, "strdup: %m");
		free(k);
		free(v);
		return -1;
	}
	v->type = type;
	v->fd = fd;
	v->index = -1;
	g_hash_table_replace(io_map, k, v);
	return 0;
}

static gboolean parse_wait_exit_arg(const gchar *value)
{
	return TRUE;
}

static gboolean parse_wait_end_arg(const gchar *value)
{
	return TRUE;
}

static gboolean parse_stderr_arg(const gchar *value)
{
	int fd = cmd_get_fd(value, O_WRONLY | O_CREAT);
	if (fd < 0) {
		neb_syslog(LOG_ERR, "Invalid io stderr value %s: open/dup failed", value);
		return FALSE;
	}
	if (io_map_replace("stderr", value, io_type_stderr, fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to set io stderr value %s", value);
		close(fd);
		return FALSE;
	}
	return TRUE;
}

static gboolean parse_stdout_arg(const gchar *value)
{
	int fd = cmd_get_fd(value, O_WRONLY | O_CREAT);
	if (fd < 0) {
		neb_syslog(LOG_ERR, "Invalid io stdout value %s: open/dup failed", value);
		return FALSE;
	}
	if (io_map_replace("stdout", value, io_type_stdout, fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to set io stdout value %s", value);
		close(fd);
		return FALSE;
	}
	return TRUE;
}

static gboolean parse_stdin_arg(const gchar *value)
{
	int fd = cmd_get_fd(value, O_RDONLY);
	if (fd < 0) {
		neb_syslog(LOG_ERR, "Invalid io stdin value %s: open/dup failed", value);
		return FALSE;
	}
	if (io_map_replace("stdin", value, io_type_stdin, fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to set io stdin value %s", value);
		close(fd);
		return FALSE;
	}
	return TRUE;
}

gboolean jmsg_parse_io_args(const gchar *option, const gchar *value, gpointer data _nattr_unused, GError **error _nattr_unused)
{
	size_t opt_len = strlen(option);
	if (opt_len <= 2 || option[0] != '-' || option[1] != '-') {
		neb_syslog(LOG_ERR, "Invalid env option %s: we only support long options", option);
		return FALSE;
	}
	option += 2;
	opt_len -= 2;

	int parse_ok = 0;
	switch (opt_len) {
	case 5:
		if (strcmp(option, "stdin") == 0)
			return parse_stdin_arg(value);
		break;
	case 6:
		if (strcmp(option, "stdout") == 0)
			return parse_stdout_arg(value);
		else if (strcmp(option, "stderr") == 0)
			return parse_stderr_arg(value);
		break;
	case 8:
		if (strcmp(option, "wait-end") == 0)
			return parse_wait_end_arg(value);
		break;
	case 9:
		if (strcmp(option, "wait-exit") == 0)
			return parse_wait_exit_arg(value);
		break;
	case 11:
		if (strcmp(option, "use-default") == 0) {
			use_default_io = 1;
			return TRUE;
		}
		break;
	default:
		break;
	}
	if (!parse_ok) {
		neb_syslog(LOG_ERR, "Invalid io option %s: no such option", option);
		return FALSE;
	}

	return TRUE;
}

int jmsg_io_init()
{
	io_map = g_hash_table_new_full(g_str_hash, g_str_equal, free, io_map_v_free);
	if (!io_map) {
		neb_syslog(LOG_ERR, "g_hash_table_new_full: failed");
		return -1;
	}

	use_default_io = 0;
	return 0;
}

void jmsg_io_deinit()
{
	if (io_map) {
		g_hash_table_destroy(io_map);
		io_map = NULL;
	}

	use_default_io = 0;
}

struct cb_param {
	json_t *io_obj;
	int err;
	int *fds;
	int fd_num;
};

static void io_set_foreach(gpointer key, gpointer value, gpointer data)
{
	struct cb_param *p = data;
	struct io_map_v *v = value;
	const char *k = key;

	if (p->err)
		return;
	json_t *io_obj = p->io_obj;

	if (v->index >= 0 || v->fd < 0) {
		neb_syslog(LOG_INFO, "Skipped io entry: %s:%s, fd %d, index %d", k, v->orig_value, v->fd, v->index);
		return;
	}
	if (use_default_io && (v->type == io_type_stdout || v->type == io_type_stderr)) {
		neb_syslog(LOG_INFO, "Skipped %s io entry %s:%s, as use-default io is set", v->type, k, v->orig_value);
		return;
	}
	json_t *type_obj = json_string(v->type);
	if (!type_obj) {
		neb_syslog(LOG_ERR, "Failed to get new json string for io %s:%s", k, v->orig_value);
		p->err = 1;
		return;
	}
	if (p->fd_num + 1 >= SRUN_MSG_MAX_FDS) {
		neb_syslog(LOG_ERR, "Too many io, max allowed is %u", SRUN_MSG_MAX_FDS);
		p->err = 1;
		json_decref(type_obj);
		return;
	}
	if (json_array_append_new(io_obj, type_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add %s:%s to io", k, v->orig_value);
		p->err = 1;
		json_decref(type_obj);
		return;
	}
	v->index = p->fd_num;
	p->fds[p->fd_num] = v->fd;
	p->fd_num++;
	v->fd = -1;
}

int jmsg_io_set(json_t *exec_msg, int fds[SRUN_MSG_MAX_FDS], int *fd_num)
{
	json_t *io_obj = json_array();
	if (!io_obj) {
		neb_syslog(LOG_ERR, "Failed to get new json array for io");
		return -1;
	}

	if (io_map && g_hash_table_size(io_map)) {
		struct cb_param param = {
			.io_obj = io_obj,
			.err = 0,
			.fds = fds,
			.fd_num = 0,
		};
		g_hash_table_foreach(io_map, io_set_foreach, &param);
		*fd_num = param.fd_num;
	} else {
		*fd_num = 0;
	}

	if (use_default_io) {
		json_t *dft_obj = json_string(io_type_dft);
		if (!dft_obj) {
			neb_syslog(LOG_ERR, "Failed to get new json string for dft io");
			json_decref(io_obj);
			return -1;
		}
		if (json_array_append_new(io_obj, dft_obj) != 0) {
			neb_syslog(LOG_ERR, "Failed to add dft io");
			json_decref(dft_obj);
			json_decref(io_obj);
			return -1;
		}
	}

	if (!json_array_size(io_obj)) {
		json_decref(io_obj);
		return 0;
	}

	if (json_object_set_new(exec_msg, "io", io_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add io to root obj");
		json_decref(io_obj);
		return -1;
	}
	return 0;
}
