
#include <nebase/syslog.h>

#include "jmsg.h"

#include <string.h>
#include <stdlib.h>

static json_t *envp_obj = NULL;

static int add_new_envp(const char *name, const char *value)
{
	if (!envp_obj) {
		neb_syslog(LOG_ERR, "envp object is empty");
		return -1;
	}
	json_t *obj = json_sprintf("%s=%s", name, value);
	if (!obj) {
		neb_syslog(LOG_ERR, "Failed to get new json string");
		return -1;
	}
	if (json_array_append_new(envp_obj, obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add %s to envp", json_string_value(obj));
		json_decref(obj);
		return -1;
	}
	return 0;
}

static gboolean parse_pass_arg(const char *value)
{
	char *v = strdup(value);
	if (!v) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return FALSE;
	}

	const char delim[] = ",";
	char *saveptr = NULL;
	gboolean ret = TRUE;
	for (const char *name = strtok_r(v, delim, &saveptr); name; name = strtok_r(NULL, delim, &saveptr)) {
		const char *env_value = getenv(name);
		if (!env_value) {
			neb_syslog(LOG_ERR, "No env named %s found", name);
			ret = FALSE;
			break;
		}
		if (add_new_envp(name, env_value) != 0) {
			neb_syslog(LOG_ERR, "Failed to add local env %s", name);
			ret = FALSE;
			break;
		}
	}

	free(v);
	return ret;
}

static gboolean parse_add_arg(const char *value)
{
	char *v = strdup(value);
	if (!v) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return FALSE;
	}

	char *pos = strchr(v, '=');
	if (!pos || !pos[1]) {
		neb_syslog(LOG_ERR, "Invalid env to add: %s", value);
		free(v);
		return FALSE;
	}
	*pos = '\0';
	gboolean ret = TRUE;
	if (add_new_envp(v, pos + 1) != 0) {
		neb_syslog(LOG_ERR, "Failed to add new env %s", value);
		ret = FALSE;
	}
	free(v);
	return ret;
}

gboolean jmsg_parse_env_args(const gchar *option, const gchar *value, gpointer data _nattr_unused, GError **error _nattr_unused)
{
	size_t opt_len = strlen(option);
	if (opt_len <= 2 || option[0] != '-' || option[1] != '-') {
		neb_syslog(LOG_ERR, "Invalid env option %s: we only support long options", option);
		return FALSE;
	}
	option += 2;
	opt_len -= 2;

	int parse_ok = 0;
	switch (opt_len) {
	case 3:
		if (strcmp(option, "add") == 0)
			return parse_add_arg(value);
		break;
	case 4:
		if (strcmp(option, "pass") == 0)
			return parse_pass_arg(value);
		break;
	default:
		break;
	}
	if (!parse_ok) {
		neb_syslog(LOG_ERR, "Invalid env option %s: no such option", option);
		return FALSE;
	}

	return TRUE;
}

int jmsg_env_init(void)
{
	envp_obj = json_array();
	if (!envp_obj) {
		neb_syslog(LOG_ERR, "Failed to get new json array for env");
		return -1;
	}
	return 0;
}

void jmsg_env_deinit(void)
{
	if (envp_obj) {
		json_decref(envp_obj);
		envp_obj = NULL;
	}
}

int jmsg_env_set(json_t *exec_msg)
{
	if (!envp_obj || !json_array_size(envp_obj))
		return 0;
	// ref set
	if (json_object_set(exec_msg, "envp", envp_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add envp to root obj");
		return -1;
	}
	return 0;
}
