
#include <nebase/syslog.h>

#include "jmsg.h"

#include <string.h>

#include <uuid.h>

static json_t *conc_obj = NULL;

static json_t *get_conc(void)
{
	if (!conc_obj) {
		neb_syslog(LOG_ERR, "conc object is empty");
		return NULL;
	}
	return conc_obj;
}

static gboolean parse_id_arg(const gchar *value)
{
	json_t *cobj = get_conc();
	if (!cobj)
		return FALSE;

	uuid_t id;
	if (uuid_parse(value, id) != 0) {
		neb_syslog(LOG_ERR, "Invalid conc uuid value: %s", value);
		return FALSE;
	}

	json_t *obj = json_string(value);
	if (!obj) {
		neb_syslog(LOG_ERR, "Failed to get new json string");
		return FALSE;
	}
	if (json_object_set_new(cobj, "id", obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add id to concurrency");
		json_decref(obj);
		return FALSE;
	}

	return TRUE;
}

static gboolean parse_num_arg(const gchar *value)
{
	json_t *cobj = get_conc();
	if (!cobj)
		return FALSE;

	char *endptr;
	int num = strtol(value, &endptr, 10);
	if (endptr && *endptr) {
		neb_syslog(LOG_ERR, "Invalid conc num value %s: not a number", value);
		return FALSE;
	}
	if (num < 0) {
		neb_syslog(LOG_ERR, "Invalid conc num value %s: out of range", value);
		return FALSE;
	}

	json_t *obj = json_integer(num);
	if (!obj) {
		neb_syslog(LOG_ERR, "Failed to get new json integer");
		return FALSE;
	}
	if (json_object_set_new(cobj, "num", obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add num to concurrency");
		json_decref(obj);
		return FALSE;
	}

	return TRUE;
}

static gboolean parse_timeout_arg(const gchar *value)
{
	json_t *cobj = get_conc();
	if (!cobj)
		return FALSE;

	char *endptr;
	int timeout = strtol(value, &endptr, 10);
	if (endptr && *endptr) {
		neb_syslog(LOG_ERR, "Invalid conc timeout value %s: not a number", value);
		return FALSE;
	}
	if (timeout < 0)
		timeout = -1;

	json_t *obj = json_integer(timeout);
	if (!obj) {
		neb_syslog(LOG_ERR, "Failed to get new json integer");
		return FALSE;
	}
	if (json_object_set_new(cobj, "timeout", obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add timeout to concurrency");
		json_decref(obj);
		return FALSE;
	}

	return TRUE;
}

gboolean jmsg_parse_conc_args(const gchar *option, const gchar *value, gpointer data _nattr_unused, GError **error _nattr_unused)
{
	size_t opt_len = strlen(option);
	if (opt_len <= 2 || option[0] != '-' || option[1] != '-') {
		neb_syslog(LOG_ERR, "Invalid conc option %s: we only support long options", option);
		return FALSE;
	}
	option += 2;
	opt_len -= 2;

	int parse_ok = 0;
	switch (opt_len) {
	case 2:
		if (strcmp(option, "id") == 0)
			return parse_id_arg(value);
		break;
	case 3:
		if (strcmp(option, "num") == 0)
			return parse_num_arg(value);
		break;
	case 7:
		if (strcmp(option, "timeout") == 0)
			return parse_timeout_arg(value);
		break;
	default:
		break;
	}
	if (!parse_ok) {
		neb_syslog(LOG_ERR, "Invalid conc option %s: no such option", option);
		return FALSE;
	}

	return TRUE;
}

int jmsg_conc_init(void)
{
	conc_obj = json_object();
	if (!conc_obj) {
		neb_syslog(LOG_ERR, "Failed to get new json map for conc");
		return -1;
	}
	return 0;
}

void jmsg_conc_deinit(void)
{
	if (conc_obj) {
		json_decref(conc_obj);
		conc_obj = NULL;
	}
}

int jmsg_conc_set(json_t *exec_msg)
{
	if (!conc_obj || !json_object_size(conc_obj))
		return 0;
	// ref set
	if (json_object_set(exec_msg, "concurrency", conc_obj) != 0) {
		neb_syslog(LOG_ERR, "Failed to add concurrency to root obj");
		return -1;
	}
	return 0;
}
