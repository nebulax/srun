
#include <nebase/syslog.h>

#include <srun/protocol.h>

#include "jmsg.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <glib.h>

static gboolean dryrun = FALSE;

static GOptionEntry main_entries[] = {
	{"socket", 's', G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME, &srund_addr, "srund socket path", "path"},
	{"dry-run", 'n', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, &dryrun, "dry run, just dump the message", NULL},
	{NULL, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, NULL, NULL}
};

static GOptionEntry envp_entries[] = {
	{"add", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK, jmsg_parse_env_args, "entry to add", "NAME=VALUE"},
	{"pass", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK, jmsg_parse_env_args, "entries to pass", "NAME[,...]"},
	{NULL, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, NULL, NULL}
};

static GOptionEntry io_entries[] = {
	{"use-default", '\0', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "use default io", NULL},
	{"stdin", '\0', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "default to @stdin", "File|@refer"},
	{"stdout", '\0', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "default to @stdout", "File|@refer"},
	{"stderr", '\0', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "default to @stderr", "File|@refer"},
	{"wait-exit", '\0', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "wait exit", "Seconds"},
	{"wait-end", '\0', G_OPTION_FLAG_OPTIONAL_ARG, G_OPTION_ARG_CALLBACK, jmsg_parse_io_args, "wait end", "Seconds"},
	{NULL, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, NULL, NULL}
};

static GOptionEntry wait_entries[] = {
	{NULL, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, NULL, NULL}
};

static GOptionEntry conc_entries[] = {
	{"id", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK, jmsg_parse_conc_args, "id", "UUID"},
	{"num", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK, jmsg_parse_conc_args, "limit number", "NUMBER"},
	{"timeout", '\0', G_OPTION_FLAG_NONE, G_OPTION_ARG_CALLBACK, jmsg_parse_conc_args, "wait timeout", "Micro Seconds"},
	{NULL, 0, G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, NULL, NULL}
};

static GOptionContext *get_args_parser(void)
{
	GOptionContext *context = g_option_context_new("-- cmd [args...]");
	g_option_context_add_main_entries(context, main_entries, NULL);

	GOptionGroup *envp_options = g_option_group_new("env", "Option for setting environment", "Show all environment options", NULL, NULL);
	g_option_group_add_entries(envp_options, envp_entries);
	g_option_context_add_group(context, envp_options);

	GOptionGroup *io_options = g_option_group_new("io", "Option for setting IO", "Show all IO options", NULL, NULL);
	g_option_group_add_entries(io_options, io_entries);
	g_option_context_add_group(context, io_options);

	GOptionGroup *wait_options = g_option_group_new("wait", "Option for setting wait conditions", "Show all wait condition options", NULL, NULL);
	g_option_group_add_entries(wait_options, wait_entries);
	g_option_context_add_group(context, wait_options);

	GOptionGroup *conc_options = g_option_group_new("conc", "Option for setting concurrency", "Show all concurrency options", NULL, NULL);
	g_option_group_add_entries(conc_options, conc_entries);
	g_option_context_add_group(context, conc_options);

	return context;
}

int main(int argc, char *argv[])
{
	neb_syslog_init(NEB_LOG_STDIO, argv[0]);

	if (jmsg_init() != 0)
		return -1;

	GError *err = NULL;
	GOptionContext *context = get_args_parser();
	if (!g_option_context_parse(context, &argc, &argv, &err)) {
		g_print("Failed to parse options: %s\n", err->message);
		jmsg_deinit();
		return -1;
	}
	g_option_context_free(context);

	if (argc > 1 && strcmp(argv[1], "--") == 0) {
		argc--;
		argv[1] = argv[0];
		argv++;
	}
	if (argc < 2) {
		fprintf(stderr, "No command given\n");
		jmsg_deinit();
		return -1;
	}
	json_t *exec_msg = jmsg_new(argc - 1, argv + 1);
	if (!exec_msg) {
		fprintf(stderr, "Failed to set cmd\n");
		jmsg_deinit();
		return -1;
	}
	int fd_num = 0;
	int fds[SRUN_MSG_MAX_FDS];
	if (jmsg_finalize(exec_msg, fds, &fd_num) != 0) {
		fprintf(stderr, "Failed to finalize jmsg\n");
		jmsg_del(exec_msg, fds, fd_num);
		jmsg_deinit();
		return -1;
	}

	int ret = 0;
	if (dryrun)
		jmsg_dump(stdout, exec_msg);
	else
		ret = jmsg_send(exec_msg, fds, fd_num);

	jmsg_del(exec_msg, fds, fd_num);

	jmsg_deinit();
	return ret;
}
