
#ifndef SRUN_JMSG_H
#define SRUN_JMSG_H 1

#include <nebase/cdefs.h>
#include <srun/message.h>

#include <stdio.h>

#include <glib.h>
#include <jansson.h>

extern int jmsg_init(void);
extern void jmsg_deinit(void);

extern json_t *jmsg_new(int argc, char *argv[]);
/**
 * \note when return failed, fds should also be closed by caller
 */
extern int jmsg_finalize(json_t *exec_msg, int fds[SRUN_MSG_MAX_FDS], int *fd_num)
	_nattr_nonnull((1, 2, 3));
extern void jmsg_del(json_t *exec_msg, int *fds, int fd_num)
	_nattr_nonnull((1));

extern void jmsg_dump(FILE *stream, json_t *exec_msg)
	_nattr_nonnull((1, 2));
extern int jmsg_send(json_t *exec_msg, int *fds, int fd_num)
	_nattr_nonnull((1));

/*
 * Command Line Parsers
 */

extern gboolean jmsg_parse_env_args(const gchar *option, const gchar *value, gpointer data, GError **error);
extern gboolean jmsg_parse_io_args(const gchar *option, const gchar *value, gpointer data, GError **error);
extern gboolean jmsg_parse_wait_args(const gchar *option, const gchar *value, gpointer data, GError **error);
extern gboolean jmsg_parse_conc_args(const gchar *option, const gchar *value, gpointer data, GError **error);

/*
 * Internal Helpers
 */

// env
extern int jmsg_env_init(void);
extern void jmsg_env_deinit(void);
extern int jmsg_env_set(json_t *exec_msg)
	_nattr_nonnull((1));

// io
extern int jmsg_io_init(void);
extern void jmsg_io_deinit(void);
/**
 * \note when return failed, fds should also be closed by caller
 */
extern int jmsg_io_set(json_t *exec_msg, int fds[SRUN_MSG_MAX_FDS], int *fd_num)
	_nattr_nonnull((1, 2, 3));

// conc
extern int jmsg_conc_init(void);
extern void jmsg_conc_deinit(void);
extern int jmsg_conc_set(json_t *exec_msg)
	_nattr_nonnull((1));

#endif
