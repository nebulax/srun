
#ifndef SRUN_PROC_H
#define SRUN_PROC_H 1

#include <nebase/cdefs.h>

#include <sys/types.h>

extern int sr_proc_quit_wait(pid_t pid, const char *name)
	_nattr_nonnull((2));

extern int sr_proc_sem_create(void)
	_nattr_warn_unused_result;
extern void sr_proc_sem_destroy(void);
extern int sr_proc_sem_child_set_ready(void)
	_nattr_warn_unused_result;
extern int sr_proc_sem_wait_child(int num)
	_nattr_warn_unused_result;
extern int sr_proc_sem_wait_daemon(void)
	_nattr_warn_unused_result;

#endif
