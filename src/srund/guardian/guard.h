
#ifndef SR_GUD_GUARD_H
#define SR_GUD_GUARD_H 1

#include <nebase/cdefs.h>

#include "unpack.h"

struct sr_gud_extra_param {
	pid_t cpid;
	int pty_master;
	int pty_remote;
	int sync_pipe;
};

extern int sr_gud_guard_run(sr_exec_t *se, const struct sr_gud_extra_param *sgep)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

#endif
