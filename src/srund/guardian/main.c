
#include <nebase/syslog.h>
#include <nebase/io.h>
#include <nebase/pipe.h>
#include <nebase/pty.h>

#include "main.h"
#include "sync.h"
#include "guard.h"
#include "signal.h"
#include "target.h"

#include <sys/types.h>
#include <unistd.h>

int sr_gud_sem = 0;
int sr_gud_sem_to_close = 0;
int sr_gud_prepare_status = SR_GUD_PREPARE_NOT_STARTED;

int sr_gud_exec(sr_exec_t *se, int unprivileged)
{
	int ret = 0;
	int pty_remote = -1;
	int pty_master = -1;
	int pty_slave = -1;

	sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_STARTED;

	if (se->orphan) {
		sr_exec_t_close_io(se);
		if (execve(se->filename, se->argv, se->envp) == -1) {
			neb_syslog(LOG_ERR, "execve(%s): %m", se->filename);
			sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FEXECVE;
			goto setup_error;
		}
	}

	if (se->pty_index >= 0) {
		if (neb_pty_openpty(&pty_master, &pty_slave) == -1) {
			neb_syslog(LOG_ERR, "Failed to open pty");
			sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FOPENPTY;
			goto setup_error;
		}
		pty_remote = se->io[se->pty_index].fd;
		se->io[se->pty_index].fd = -1;
	}

	// TODO setup subreaper

	sr_gud_sem = neb_sem_proc_create(NULL, SR_GUD_SEM_COUNT);
	if (sr_gud_sem < 0) {
		neb_syslog(LOG_ERR, "Failed to create proc semaphore");
		sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FSEMCREATE;
		goto setup_error;
	}
	sr_gud_sem_to_close = 1;

	int pipefd[2];
	if (neb_pipe_new(pipefd) != 0) {
		neb_syslog(LOG_ERR, "Failed to create sync pipe");
		sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FPIPECREATE;
		goto setup_error;
	}

	if (sr_gud_signal_block_all() != 0) {
		neb_syslog(LOG_ERR, "Failed to block signals");
		sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FSETSIGMASK;
		goto setup_error;
	}

	pid_t cpid = fork();
	if (cpid == -1) {
		neb_syslog(LOG_ERR, "fork: %m");
		sr_gud_prepare_status = SR_GUD_PREPARE_PREFORK_FFORK;
		goto setup_error;
	} else if (cpid == 0) {
		if (pty_remote >= 0) {
			close(pty_remote);
			pty_remote = -1;
		}
		if (pty_master >= 0) {
			close(pty_master);
			pty_master = -1;
		}
		if (pty_slave >= 0) {
			se->io[se->pty_index].fd = pty_slave;
			pty_slave = -1;
		}
		sr_gud_sem_to_close = 0;
		close(pipefd[0]);

		ret = sr_gud_target_run(se, pipefd[1], unprivileged);
	} else {
		if (pty_slave >= 0) {
			close(pty_slave);
			pty_slave = -1;
		}

		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_STARTED;

		close(pipefd[1]);
		sr_exec_t_close_io(se);

		struct sr_gud_extra_param sgep = {
			.cpid = cpid,
			.pty_master = pty_master,
			.pty_remote = pty_remote,
			.sync_pipe = pipefd[0],
		};
		ret = sr_gud_guard_run(se, &sgep);
	}

exit_close_pty:
	if (sr_gud_sem_to_close) {
		neb_sem_proc_destroy(sr_gud_sem);
		sr_gud_sem_to_close = 0;
	}
	if (pty_remote >= 0) {
		close(pty_remote);
	}
	if (pty_master >= 0) {
		close(pty_master);
	}
	sr_exec_t_free(se);
	return ret;
setup_error:
	// TODO handle db
	ret = -1;
	goto exit_close_pty;
}
