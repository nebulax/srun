
#include <nebase/syslog.h>

#include "target.h"
#include "sync.h"
#include "signal.h"
#include "ctl/io.h"

#include <unistd.h>
#include <fcntl.h>

static int child_preexec(sr_exec_t *se, int unprivileged)
{
	return 0;
}

static int set_task_prepare_status(int notify_pipe)
{
	int prepare_status = sr_gud_prepare_status;
	ssize_t nw = write(notify_pipe, &prepare_status, sizeof(prepare_status));
	if (nw == -1) {
		neb_syslog(LOG_ERR, "write: %m");
		return -1;
	}
	return 0;
}

int sr_gud_target_run(sr_exec_t *se, int notify_pipe, int unprivileged)
{
	struct timespec ts = {.tv_sec = 1};
	if (neb_sem_proc_wait_count(sr_gud_sem, SR_GUD_SEM_WAIT_PARENT, 1, &ts) != 0) {
		neb_syslog(LOG_ERR, "Parent is not ready after timeout, task quit");
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FWAITPARENT;
		goto setup_error;
	}
	/*
	 * Always put the target process in a new session,
	 * which has no controlling terminal by default.
	 */
	if (setsid() == -1) {
		neb_syslog(LOG_ERR, "setsid: %m");
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FSETSID;
		goto setup_error;
	}

	if (child_preexec(se, unprivileged) != 0) {
		neb_syslog(LOG_ERR, "Failed to setup before exec");
		goto setup_error;
	}

	// TODO handle wait and concurrency

	if (sr_io_do_redirect(se) != 0) {
		neb_syslog(LOG_ERR, "Failed to do io redirection");
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FIOREDIRECT;
		goto setup_error;
	}

	if (fcntl(notify_pipe, F_SETFD, FD_CLOEXEC) == -1) {
		neb_syslog(LOG_ERR, "fcntl(F_SETFD, FD_CLOEXEC): %m");
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FSETCLOEXEC;
		goto setup_error;
	}
	sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_PREEXECVEPE;
	if (set_task_prepare_status(notify_pipe) != 0) {
		neb_syslog(LOG_ERR, "Failed to notify parent prepare_status %d", sr_gud_prepare_status);
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FNOTIFY;
		goto setup_error;
	}

	if (sr_gud_signal_unblock_all() != 0) {
		neb_syslog(LOG_ERR, "");
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FCLRSIGMASK;
		goto setup_error;
	}

	if (execvpe(se->filename, se->argv, se->envp) == -1) {
		neb_syslog(LOG_ERR, "execvpe(%s): %m", se->filename);
		sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_FEXECVEPE;
		goto setup_error;
	}
	return 0;

setup_error:
	if (set_task_prepare_status(notify_pipe) != 0)
		neb_syslog(LOG_ERR, "Failed to notify parent prepare_status %d", sr_gud_prepare_status);
	return -1;
}
