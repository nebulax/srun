
#include <nebase/syslog.h>
#include <srun/message.h>

#include "unpack.h"
#include "ctl/sched.h"

#include <stdlib.h>
#include <unistd.h>

#include <msgpack.h>
#include <uuid.h>

uid_t sr_cred_uid_default = 0;
gid_t sr_cred_gid_default = 0;

static int msg_unpack_argv(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_ARRAY) {
		neb_syslog(LOG_WARNING, "Value of argv is not array");
		return -1;
	}

	if (se->argv) {
		for (int i = 0; i < se->argc; i++) {
			char *p = se->argv[i];
			if (p)
				free(p);
		}
		free(se->argv);
		se->argv = NULL;
	}

	if (!vobj->via.array.size)
		return 0;

	se->argc = vobj->via.array.size + 1; // including filename
	se->argv = calloc(se->argc + 1, sizeof(char *));
	if (!se->argv) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return -1;
	}

	if (se->filename) {
		se->argv[0] = strdup(se->filename);
		if (!se->argv[0]) {
			neb_syslog(LOG_ERR, "strdup: %m");
			return -1;
		}
	}
	for (int i = 1; i < se->argc; i++) {
		msgpack_object *obj = vobj->via.array.ptr + i - 1;
		if (obj->type != MSGPACK_OBJECT_STR || !obj->via.str.size) {
			neb_syslog(LOG_WARNING, "Value of argv/%d is not str", i);
			return -1;
		}

		se->argv[i] = strndup(obj->via.str.ptr, obj->via.str.size);
		if (!se->argv[i]) {
			neb_syslog(LOG_ERR, "strndup: %m");
			return -1;
		}
	}

	return 0;
}

static int msg_unpack_envp(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_ARRAY) {
		neb_syslog(LOG_WARNING, "Value of envp is not array");
		return -1;
	}

	if (se->envp) {
		for (char *p = se->envp[0]; p; p++)
			free(p);
		free(se->envp);
		se->envp = NULL;
	}

	if (!vobj->via.array.size)
		return 0;

	int envc = vobj->via.array.size;
	se->envp = calloc(envc + 1, sizeof(char *));
	if (!se->envp) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return -1;
	}

	for (int i = 0; i < envc; i++) {
		msgpack_object *obj = vobj->via.array.ptr + i;
		if (obj->type != MSGPACK_OBJECT_STR || !obj->via.str.size) {
			neb_syslog(LOG_WARNING, "Value of envp/%d is not str", i);
			return -1;
		}

		se->envp[i] = strndup(obj->via.str.ptr, obj->via.str.size);
		if (!se->envp[i]) {
			neb_syslog(LOG_ERR, "strndup: %m");
			return -1;
		}
	}

	return 0;
}

static int check_io_type_conflict(sr_exec_t *se, int nindex, int ntype)
{
	for (int i = 0; i < nindex; i++) {
		int otype = se->io[i].type;
		if (io_type_is_conflict(ntype, otype)) {
			neb_syslog(LOG_WARNING, "IO Type Conflice: %d/%s - %d/%s", i, io_type_str(otype), nindex, io_type_str(ntype));
			return -1;
		}
	}
	return 0;
}

static int msg_unpack_io(msgpack_object *vobj, sr_exec_t *se, int *fds, int fd_num)
{
	if (vobj->type != MSGPACK_OBJECT_ARRAY) {
		neb_syslog(LOG_WARNING, "Value of io is not array");
		return -1;
	}
	int io_size = vobj->via.array.size;
	if (io_size < fd_num) {
		neb_syslog(LOG_WARNING, "io array size(%d) is less than fd_num(%d)", io_size, fd_num);
		return -1;
	}

	int fdi = 0;
	for (int i = 0; i < io_size; i++) {
		msgpack_object *obj = vobj->via.array.ptr + i;
		if (obj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_ERR, "Value of io/%d is not integer", i);
			return -1;
		}

		int io_type = obj->via.i64;
		if (check_io_type_conflict(se, i, io_type) != 0)
			return -1;
		switch (io_type) {
		case SRUN_MSG_IO_TYPE_PTY:
			se->pty_index = i; /* falls through */
		case SRUN_MSG_IO_TYPE_KEEP:
		case SRUN_MSG_IO_TYPE_PASS:
		case SRUN_MSG_IO_TYPE_STDERR:
		case SRUN_MSG_IO_TYPE_STDIN:
		case SRUN_MSG_IO_TYPE_STDOUT:
			if (fdi >= fd_num) {
				neb_syslog(LOG_ERR, "There is not enough fd_num(%d)", fd_num);
				return -1;
			}
			se->io[i].type = io_type;
			se->io[i].fd = fds[fdi];
			fds[fdi] = -1;
			fdi++;
			break;
		case SRUN_MSG_IO_TYPE_DFT:
			se->io[i].type = io_type;
			se->io[i].fd = -1;
			se->dft_index = i;
			break;
		case SRUN_MSG_IO_TYPE_NONE:
		default:
			neb_syslog(LOG_ERR, "Invalid io type %d for io/%d", io_type, i);
			return -1;
			break;
		}
	}

	se->io_count = io_size;
	return 0;
}

static int msg_unpack_wait(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_MAP) {
		neb_syslog(LOG_WARNING, "Value of wait is not map");
		return -1;
	}

	if (!vobj->via.map.size)
		return 0;
	// no map_size check here, the last one will overwrite previous one

	for (uint32_t i = 0; i < vobj->via.map.size; i++) {
		msgpack_object_kv *subobj = vobj->via.map.ptr + i;
		msgpack_object *subkobj = &subobj->key;
		if (subkobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Key of wait/%u is not integer", i);
			return -1;
		}
		msgpack_object *subvobj = &subobj->val;

		int index = 0, signo = 0;
		switch (subkobj->via.i64) {
		case SRUN_MSG_WAIT_KEY_TIMEOUT:
			switch (subvobj->type) {
			case MSGPACK_OBJECT_POSITIVE_INTEGER:
				se->wait.timeout_ms = subvobj->via.i64;
				break;
			case MSGPACK_OBJECT_NEGATIVE_INTEGER:
				se->wait.timeout_ms = -1;
				break;
			default:
				neb_syslog(LOG_WARNING, "Value of wait/timeout is not integer");
				return -1;
				break;
			}
			break;
		case SRUN_MSG_WAIT_KEY_SIGNAL:
			if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
				neb_syslog(LOG_WARNING, "Value of wait/signal is not integer");
				return -1;
			}
			signo = subvobj->via.i64;
			if (signo < SRUN_SIGNO_MIN || signo > SRUN_SIGNO_MAX) {
				neb_syslog(LOG_WARNING, "Value of wait/signal %d out of range(%u-%u)", signo, SRUN_SIGNO_MIN, SRUN_SIGNO_MAX);
				return -1;
			}
			se->wait.type = SR_WAIT_TYPE_SIGNAL;
			se->wait.value = signo;
			break;
		case SRUN_MSG_WAIT_KEY_FDIN:
			if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
				neb_syslog(LOG_WARNING, "Value of wait/fdin is not integer");
				return -1;
			}
			index = subvobj->via.i64;
			if (index < 0 || index >= SRUN_MSG_MAX_FDS) {
				neb_syslog(LOG_WARNING, "Value of wait/fdin %d out of range(0-%u)", index, SRUN_MSG_MAX_FDS);
				return -1;
			}
			se->wait.type = SR_WAIT_TYPE_FDIN;
			se->wait.value = index;
			break;
		case SRUN_MSG_WAIT_KEY_FDHUP:
			if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
				neb_syslog(LOG_WARNING, "Value of wait/fdhup is not integer");
				return -1;
			}
			index = subvobj->via.i64;
			if (index < 0 || index >= SRUN_MSG_MAX_FDS) {
				neb_syslog(LOG_WARNING, "Value of wait/fdhup %d out of range(0-%u)", index, SRUN_MSG_MAX_FDS);
				return -1;
			}
			se->wait.type = SR_WAIT_TYPE_FDHUP;
			se->wait.value = index;
			break;
		default:
			neb_syslog(LOG_WARNING, "Unknown wait key value: %ld", subkobj->via.i64);
			return -1;
		}
	}

	return 0;
}

static int msg_unpack_concurrency(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_MAP) {
		neb_syslog(LOG_WARNING, "Value of concurrency is not map");
		return -1;
	}

	if (!vobj->via.map.size)
		return 0;

	for (uint32_t i = 0; i < vobj->via.map.size; i++) {
		msgpack_object_kv *subobj = vobj->via.map.ptr + i;
		msgpack_object *subkobj = &subobj->key;
		if (subkobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Key of concurrency/%u is not integer", i);
			return -1;
		}
		msgpack_object *subvobj = &subobj->val;

		switch (subkobj->via.i64) {
		case SRUN_MSG_CONCURRENCY_KEY_ID:
			if (subvobj->type != MSGPACK_OBJECT_BIN) {
				neb_syslog(LOG_WARNING, "Value of concurrency/id is not bin of uuid");
				return -1;
			}
			if (subvobj->via.bin.size != sizeof(uuid_t)) {
				neb_syslog(LOG_WARNING, "Value of concurrency/id should be size %u", (uint)sizeof(uuid_t));
				return -1;
			}
			uuid_copy(se->concurrency.id, *(const uuid_t *)subvobj->via.bin.ptr);
			break;
		case SRUN_MSG_CONCURRENCY_KEY_NUM:
			if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
				neb_syslog(LOG_WARNING, "Value of concurrency/num is not integer");
				return -1;
			}
			se->concurrency.num = subvobj->via.i64;
			break;
		case SRUN_MSG_CONCURRENCY_KEY_TIMEOUT:
			switch (subvobj->type) {
			case MSGPACK_OBJECT_POSITIVE_INTEGER:
				se->concurrency.timeout_ms = subvobj->via.i64;
				break;
			case MSGPACK_OBJECT_NEGATIVE_INTEGER:
				se->concurrency.timeout_ms = -1;
				break;
			default:
				neb_syslog(LOG_WARNING, "Value of concurrency/timeout is not integer");
				return -1;
				break;
			}
			break;
		default:
			neb_syslog(LOG_WARNING, "Unknown concurrency key value: %ld", subkobj->via.i64);
			return -1;
			break;
		}
	}

	return 0;
}

static int msg_unpack_cred(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_MAP) {
		neb_syslog(LOG_WARNING, "Value of cred is not map");
		return -1;
	}

	se->cred.uid = sr_cred_uid_default;
	se->cred.gid = sr_cred_gid_default;

	if (!vobj->via.map.size)
		return 0;

	for (uint32_t i = 0; i < vobj->via.map.size; i++) {
		msgpack_object_kv *subobj = vobj->via.map.ptr + i;
		msgpack_object *subkobj = &subobj->key;
		if (subkobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Key of cred/%u is not integer", i);
			return -1;
		}
		msgpack_object *subvobj = &subobj->val;
		if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Value of cred/%u is not integer", i);
			return -1;
		}

		int64_t v = subvobj->via.i64;
		switch (subkobj->via.i64) {
		case SRUN_MSG_CRED_KEY_UID:
			if (v < 0 || v >= UINT16_MAX) {
				neb_syslog(LOG_WARNING, "Invalid cred uid value %ld", v);
				return -1;
			}
			se->cred.uid = v;
			break;
		case SRUN_MSG_CRED_KEY_GID:
			if (v < 0 || v >= UINT16_MAX) {
				neb_syslog(LOG_WARNING, "Invalid cred gid value %ld", v);
				return -1;
			}
			se->cred.gid = v;
			break;
		default:
			neb_syslog(LOG_WARNING, "Unknown cred key value: %ld", subkobj->via.i64);
			return -1;
			break;
		}
	}

	return 0;
}

static int msg_unpack_sched(msgpack_object *vobj, sr_exec_t *se)
{
	if (vobj->type != MSGPACK_OBJECT_MAP) {
		neb_syslog(LOG_WARNING, "Value of sched is not map");
		return -1;
	}

	if (!vobj->via.map.size)
		return 0;
	se->sched.policy = sr_sched_policy_default;
	se->sched.priority = sr_sched_priority_default;

	int policy = sr_sched_policy_default, priority = 0;
	int has_policy = 0, has_priority = 0;
	for (uint32_t i = 0; i < vobj->via.map.size; i++) {
		msgpack_object_kv *subobj = vobj->via.map.ptr + i;
		msgpack_object *subkobj = &subobj->key;
		if (subkobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Key of sched/%u is not integer", i);
			return -1;
		}
		msgpack_object *subvobj = &subobj->val;
		if (subvobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Value of sched/%u is not integer", i);
			return -1;
		}

		int64_t v = subvobj->via.i64;
		if (v < INT_MIN || v > INT_MAX) {
			neb_syslog(LOG_WARNING, "Invalid sched/* value %ld", v);
			return -1;
		}

		switch (subkobj->via.i64) {
		case SRUN_MSG_SCHED_KEY_POLICY:
			has_policy = 1;
			policy = v;
			break;
		case SRUN_MSG_SCHED_KEY_PRIORITY:
			has_priority = 1;
			priority = v;
			break;
		default:
			neb_syslog(LOG_WARNING, "Unknown sched key value: %ld", subkobj->via.i64);
			return -1;
			break;
		}
	}

	if (has_priority) {
		if (!sr_sched_policy_is_valid(policy)) {
			neb_syslog(LOG_WARNING, "Invalid sched policy %d", policy);
			return -1;
		}
		if (!sr_sched_priority_is_valid(policy, priority)) {
			neb_syslog(LOG_WARNING, "Invalid sched priority %d for policy %d", priority, policy);
			return -1;
		}
		se->sched.policy = policy;
		se->sched.priority = priority;
	} else if (has_policy) {
		neb_syslog(LOG_WARNING, "No sched priority specified for policy %d", policy);
		return -1;
	}

	return 0;
}

static int verify_wait_io(const sr_exec_t *se)
{
	int io_type = SRUN_MSG_IO_TYPE_NONE;
	switch (se->wait.type) {
	case SR_WAIT_TYPE_FDIN:
		io_type = se->io[se->wait.value].type;
		switch (io_type) {
		case SRUN_MSG_IO_TYPE_STDIN:
		case SRUN_MSG_IO_TYPE_PASS:
			break;
		default:
			neb_syslog(LOG_WARNING, "io %d: wait type is fdin, while io type is %s", se->wait.value, io_type_str(io_type));
			return -1;
			break;
		}
		break;
	case SR_WAIT_TYPE_FDHUP:
		io_type = se->io[se->wait.value].type;
		if (io_type != SRUN_MSG_IO_TYPE_PASS) {
			neb_syslog(LOG_WARNING, "io %d: wait type is fdhup, while io type is %s", se->wait.value, io_type_str(io_type));
			return -1;
		}
		break;
	default:
		break;
	}
	return 0;
}

static int get_default_argv(sr_exec_t *se)
{
	se->argv = malloc(sizeof(char *));
	if (!se->argv) {
		neb_syslog(LOG_ERR, "malloc: %m");
		return -1;
	}

	if (!se->filename) {
		neb_syslog(LOG_ERR, "No filename found");
		return -1;
	}
	se->argv[0] = strdup(se->filename);
	if (!se->argv[0]) {
		neb_syslog(LOG_ERR, "strdup: %m");
		return -1;
	}

	se->argc = 1;
	return 0;
}

sr_exec_t *sr_gud_msg_unpack(const char *data, size_t len, int *fds, int fd_num)
{
	msgpack_unpacked result;
	msgpack_unpack_return ret = MSGPACK_UNPACK_SUCCESS;
	msgpack_unpacked_init(&result);

	sr_exec_t *se = calloc(1, sizeof(sr_exec_t));
	if (!se) {
		neb_syslog(LOG_ERR, "calloc: %m");
		goto exit_return;
	}
	se->cred.uid = sr_cred_uid_default;
	se->cred.gid = sr_cred_gid_default;
	se->pty_index = -1;
	se->dft_index = -1;

	//There should be only one object, so no loop here
	size_t off = 0;
	ret = msgpack_unpack_next(&result, data, len, &off);
	if (ret != MSGPACK_UNPACK_SUCCESS) {
		neb_syslog(LOG_WARNING, "Invalid message: msgpack_unpack_next: %d", ret);
		goto exit_free_se;
	} else if (off != len) {
		neb_syslog(LOG_WARNING, "There is %lu more data left in this message, ignore them", off);
	}

	msgpack_object *mobj = &result.data;
	if (mobj->type != MSGPACK_OBJECT_MAP) {
		neb_syslog(LOG_WARNING, "Object should begin with a map");
		goto exit_free_se;
	}

	for (uint32_t i = 0; i < mobj->via.map.size; i++) {
		msgpack_object_kv *kvobj = mobj->via.map.ptr + i;
		msgpack_object *kobj = &kvobj->key;
		if (kobj->type != MSGPACK_OBJECT_POSITIVE_INTEGER) {
			neb_syslog(LOG_WARNING, "Key should be of positive integer type");
			goto exit_free_se;
		}
		msgpack_object *vobj = &kvobj->val;

		int64_t k = kobj->via.i64;
		switch (k) {
		case SRUN_MSG_ROOT_KEY_FILENAME:
			if (vobj->type != MSGPACK_OBJECT_STR || !vobj->via.str.size) {
				neb_syslog(LOG_WARNING, "Value of filename is not str");
				goto exit_free_se;
			}
			if (se->filename)
				free(se->filename);
			se->filename = strndup(vobj->via.str.ptr, vobj->via.str.size);
			if (!se->filename) {
				neb_syslog(LOG_ERR, "strdup: %m");
				goto exit_free_se;
			}
			if (se->argv) {
				if (se->argv[0])
					free(se->argv[0]);
				se->argv[0] = strdup(se->filename);
				if (!se->argv[0]) {
					neb_syslog(LOG_ERR, "strdup: %m");
					goto exit_free_se;
				}
			}
			break;
		case SRUN_MSG_ROOT_KEY_ARGV:
			if (msg_unpack_argv(vobj, se) != 0)
				goto exit_free_se;
			break;
		case SRUN_MSG_ROOT_KEY_ENVP:
			if (msg_unpack_envp(vobj, se) != 0)
				goto exit_free_se;
			break;
		case SRUN_MSG_ROOT_KEY_ORPHAN:
			if (vobj->type != MSGPACK_OBJECT_BOOLEAN) {
				neb_syslog(LOG_WARNING, "Value of orphan is not boolean");
				goto exit_free_se;
			}
			se->orphan = vobj->via.boolean;
			break;
		case SRUN_MSG_ROOT_KEY_IO:
			if (msg_unpack_io(vobj, se, fds, fd_num) != 0)
				goto exit_free_se;
			break;
		case SRUN_MSG_ROOT_KEY_WAIT:
			se->wait.set = 0;
			if (msg_unpack_wait(vobj, se) != 0)
				goto exit_free_se;
			se->wait.set = 1;
			break;
		case SRUN_MSG_ROOT_KEY_CONCURRENCY:
			se->concurrency.set = 0;
			if (msg_unpack_concurrency(vobj, se) != 0)
				goto exit_free_se;
			se->concurrency.set = 1;
			break;
		case SRUN_MSG_ROOT_KEY_CRED:
			if (msg_unpack_cred(vobj, se) != 0)
				goto exit_free_se;
			break;
		case SRUN_MSG_ROOT_KEY_SCHED:
			se->sched.set = 0;
			if (msg_unpack_sched(vobj, se) != 0)
				goto exit_free_se;
			se->sched.set = 1;
			break;
			break;
		default:
			neb_syslog(LOG_WARNING, "Unknown key value: %ld", k);
			goto exit_free_se;
			break;
		}
	}

	if (verify_wait_io(se) != 0)
		goto exit_free_se;

	if (!se->argv && get_default_argv(se) != 0)
		goto exit_free_se;

exit_return:
	msgpack_unpacked_destroy(&result);
	return se;

exit_free_se:
	sr_exec_t_free(se);
	se = NULL;
	goto exit_return;
}

void sr_exec_t_close_io(sr_exec_t *se)
{
	for (int i = 0; i < se->io_count; i++) {
		struct sr_io *iop = &se->io[i];
		if (iop->fd >= 0) {
			close(iop->fd);
			iop->type = SRUN_MSG_IO_TYPE_NONE;
			iop->fd = -1;
		}
	}
}

void sr_exec_t_free(sr_exec_t *se)
{
	if (se->filename)
		free(se->filename);
	if (se->argv) {
		for (int i = 0; i < se->argc; i++) {
			char *p = se->argv[i];
			if (p)
				free(p);
		}
		free(se->argv);
	}
	if (se->envp) {
		for (char *p = se->envp[0]; p; p++)
			free(p);
		free(se->envp);
	}
	sr_exec_t_close_io(se);

	free(se);
}
