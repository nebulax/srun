
#ifndef SR_GUD_SIGNAL_H
#define SR_GUD_SIGNAL_H 1

extern int sr_gud_signal_block_all(void);
extern int sr_gud_signal_unblock_all(void);
extern int se_gud_signal_unblock_handled(void);
extern int sr_gud_signal_set_handler(void);

#endif
