
#ifndef SRUN_GUD_MAIN_H
#define SRUN_GUD_MAIN_H 1

#include <nebase/cdefs.h>

#include "unpack.h"

extern int sr_gud_exec(sr_exec_t *se, int unprivileged)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
