
#ifndef SR_GUD_SCHED_H
#define SR_GUD_SCHED_H 1

#include <stdbool.h>

extern int sr_sched_policy_default;
extern int sr_sched_priority_default;

extern bool sr_sched_policy_is_valid(int policy);
extern bool sr_sched_priority_is_valid(int policy, int priority);

#endif
