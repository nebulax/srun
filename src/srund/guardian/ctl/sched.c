
#include <options.h>

#include "sched.h"

#include <sched.h>

int sr_sched_policy_default = SCHED_OTHER;
int sr_sched_priority_default = 0;

bool sr_sched_policy_is_valid(int policy)
{
	switch (policy) {
	case SCHED_OTHER: /* fall through */
	case SCHED_FIFO:  /* fall through */
	case SCHED_RR:    /* fall through */
#if defined(OS_LINUX)
	case SCHED_BATCH: /* fall through */
	case SCHED_ISO:   /* fall through */
	case SCHED_IDLE:  /* fall through */
	case SCHED_DEADLINE: /* fall through */
#elif defined(OS_FREEBSD)
	// no extra ones
#elif defined(OS_SOLARIS)
	case SCHED_SYS:   /* fall through */
	case SCHED_IA:    /* fall through */
	case SCHED_FSS:   /* fall through */
	case SCHED_FX:    /* fall through */
#else
# error "fix me"
#endif
		break;
	default:
		return false;
	}
	return true;
}

bool sr_sched_priority_is_valid(int policy, int priority)
{
	int min = sched_get_priority_min(policy);
	int max = sched_get_priority_max(policy);

	return (priority >= min && priority <= max);
}
