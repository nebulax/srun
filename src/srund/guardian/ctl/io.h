
#ifndef SR_GUD_IO_H
#define SR_GUD_IO_H 1

#include <nebase/cdefs.h>

#include <src/srund/shmsg.h>
#include <src/srund/guardian/unpack.h>

extern int sr_io_user_chdir(const sr_shmsg_t *sm, int *home_dirfd)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

extern int sr_io_user_open_default(const sr_shmsg_t *sm, int home_dirfd, sr_exec_t *se)
	_nattr_warn_unused_result _nattr_nonnull((1, 3));

extern int sr_io_sys_open_default(const sr_shmsg_t *sm, sr_exec_t *se)
	_nattr_warn_unused_result _nattr_nonnull((1, 2));

extern int sr_io_do_redirect(sr_exec_t *se)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
