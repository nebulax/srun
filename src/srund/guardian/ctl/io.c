
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/io.h>
#include <nebase/pipe.h>
#include <nebase/file.h>
#include <nebase/pty.h>

#include <src/srund/main.h>
#include <src/srund/hijacker/msg.h>
#include <src/srund/hijacker/main.h>

#include "io.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pwd.h>

#include <uuid.h>

static neb_file_permission_t home_dir_perm = {};

static int dir_chown_to_user(int dirfd)
{
	if (fchown(dirfd, home_dir_perm.uid, home_dir_perm.gid) == -1) {
		neb_syslog(LOG_ERR, "fchown(uid %u, gid %u): %m", home_dir_perm.uid, home_dir_perm.gid);
		return -1;
	}
	if (fchmod(dirfd, 0755) == -1) {
		neb_syslog(LOG_ERR, "fchmod: %m");
		return -1;
	}
	return 0;
}

static int get_user_home_dirfd(uid_t uid, const char *home_dir, int *home_dirfd)
{
	int enoent = 0;
	int fd = neb_dir_open(home_dir, &enoent);
	if (fd < 0) {
		if (enoent) {
			neb_syslog(LOG_NOTICE, "No valid home directory with uid %u", uid);
			*home_dirfd = -1;
			return 0;
		}
		neb_syslog(LOG_ERR, "Failed to open user home directory %s", home_dir);
		return -1;
	}

	if (neb_dirfd_get_permission(fd, &home_dir_perm) != 0) {
		neb_syslog(LOG_ERR, "Failed to get permission of %s", home_dir);
		close(fd);
		return -1;
	}

	if (home_dir_perm.uid != uid) {
		neb_syslog(LOG_NOTICE, "Directory %s is not belonged to uid %u", home_dir, uid);
		close(fd);
		*home_dirfd = -1;
		return 0;
	}

	*home_dirfd = fd;
	return 0;
}

int sr_io_user_chdir(const sr_shmsg_t *sm, int *home_dirfd)
{
	ssize_t buflen = sysconf(_SC_GETPW_R_SIZE_MAX);
	if (buflen == -1) {
		neb_syslog(LOG_ERR, "sysconf: %m");
		return -1;
	}
	char *buf = malloc(buflen);
	if (!buf) {
		neb_syslog(LOG_ERR, "malloc: %m");
		return -1;
	}

	struct passwd pwd;
	struct passwd *result;
	int err = getpwuid_r(sm->uid, &pwd, buf, buflen, &result);
	if (err != 0) {
		neb_syslog_en(err, LOG_ERR, "getpwuid_r(%u): %m", sm->uid);
		free(buf);
		return -1;
	}
	if (!result || !pwd.pw_dir) { // No user found
		neb_syslog(LOG_NOTICE, "No valid home directory with uid %u", sm->uid);
		*home_dirfd = -1;
		free(buf);
		return 0;
	}

	int ret = 0;
	if (get_user_home_dirfd(sm->uid, pwd.pw_dir, home_dirfd) != 0) {
		ret = -1;
		goto exit_free_buf;
	}
	if (*home_dirfd < 0)
		goto exit_free_buf;

	if (fchdir(*home_dirfd) == -1) {
		neb_syslog(LOG_ERR, "fchdir(%s): %m", pwd.pw_dir);
		close(*home_dirfd);
		*home_dirfd = -1;
		ret = -1;
	}

exit_free_buf:
	free(buf);
	return ret;
}

static int io_cache_open_default(const sr_shmsg_t *sm, int cache_dirfd, const char *prefix_dirname, sr_exec_t *se)
{
	int need_chown;

	int io_dirfd;
	static const char io_dirname[] = "io";
	need_chown = 0;
	do {
		int enoent = 0;
		io_dirfd = neb_subdir_open(cache_dirfd, io_dirname, &enoent);
		if (io_dirfd >= 0) {
			if (home_dir_perm.uid != 0 && need_chown && dir_chown_to_user(io_dirfd) != 0) {
				neb_syslog(LOG_ERR, "Failed to change owner of %s cache subdir", io_dirname);
				if (unlinkat(cache_dirfd, io_dirname, AT_REMOVEDIR) == -1)
					neb_syslog(LOG_ERR, "unlinkat: %m");
				return -1;
			}
			break;
		}
		if (enoent) {
			if (mkdirat(cache_dirfd, io_dirname, 0644) == -1) {
				if (errno == EEXIST)
					continue;
				neb_syslog(LOG_ERR, "mkdirat(%s/%s): %m", prefix_dirname, io_dirname);
				return -1;
			}
			need_chown = 1;
		} else {
			return -1;
		}
	} while (1);

	char uuid[UUID_STR_LEN];
	uuid_unparse(sm->uuid, uuid);

	// TODO modify logdb

	int fd = openat(io_dirfd, uuid, O_WRONLY | O_CLOEXEC | O_NONBLOCK | O_CREAT | O_EXCL, 0640);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "openat(%s/%s/%s): %m", prefix_dirname, io_dirname, uuid);
		close(io_dirfd);
		return -1;
	}
	close(io_dirfd);
	if (home_dir_perm.uid != 0 && fchown(fd, home_dir_perm.uid, home_dir_perm.gid) == -1) {
		neb_syslog(LOG_ERR, "fchown(uid %u, gid %u): %m", home_dir_perm.uid, home_dir_perm.gid);
		return -1;
	}

	int pipefd[2];
	if (neb_pipe_new(pipefd) != 0) {
		neb_syslog(LOG_ERR, "Failed to create hijacker pipefd");
		return -1;
	}

	sr_hjk_msg_t msg = {
		.type = SR_HJK_IO_TYPE_DEFAULT,
		.limit = SR_HJK_MSG_LIMIT_DEFAULT,
	};
	uuid_copy(msg.id, sm->uuid);
	if (sr_hjk_send_msg(&msg, pipefd[0], fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to send fds to hijacker");
		close(pipefd[0]);
		close(pipefd[1]);
		return -1;
	}

	se->io[se->dft_index].fd = pipefd[1];
	return 0;
}

int sr_io_user_open_default(const sr_shmsg_t *sm, int home_dirfd, sr_exec_t *se)
{
	int need_chown;

	int ucache_dirfd;
	static const char ucache_dirname[] = ".cache";
	need_chown = 0;
	do {
		int enoent = 0;
		ucache_dirfd = neb_subdir_open(home_dirfd, ucache_dirname, &enoent);
		if (ucache_dirfd >= 0) {
			if (need_chown && dir_chown_to_user(ucache_dirfd) != 0) {
				neb_syslog(LOG_ERR, "Failed to change owner of %s subdir", ucache_dirname);
				if (unlinkat(home_dirfd, ucache_dirname, AT_REMOVEDIR) == -1)
					neb_syslog(LOG_ERR, "unlinkat: %m");
				return -1;
			}
			break;
		}
		if (enoent) {
			if (mkdirat(home_dirfd, ucache_dirname, 0644) == -1) {
				if (errno == EEXIST)
					continue;
				neb_syslog(LOG_ERR, "mkdirat(~/%s): %m", ucache_dirname);
				return -1;
			}
			need_chown = 1;
		} else {
			return -1;
		}
	} while (ucache_dirfd < 0);

	int cache_dirfd;
	static const char srund_dirname[] = SRUND_DAEMON_NAME;
	need_chown = 0;
	do {
		int enoent = 0;
		cache_dirfd = neb_subdir_open(ucache_dirfd, srund_dirname, &enoent);
		if (cache_dirfd >= 0) {
			if (need_chown && dir_chown_to_user(cache_dirfd) != 0) {
				neb_syslog(LOG_ERR, "Failed to change owner of %s/%s subdir", ucache_dirname, srund_dirname);
				if (unlinkat(ucache_dirfd, srund_dirname, AT_REMOVEDIR) == -1)
					neb_syslog(LOG_ERR, "unlinkat: %m");
				close(ucache_dirfd);
				return -1;
			}
			break;
		}
		if (enoent) {
			if (mkdirat(ucache_dirfd, srund_dirname, 0644) == -1) {
				if (errno == EEXIST)
					continue;
				neb_syslog(LOG_ERR, "mkdirat(~/%s/%s): %m", ucache_dirname, srund_dirname);
				close(ucache_dirfd);
				return -1;
			}
			need_chown = 1;
		} else {
			close(ucache_dirfd);
			return -1;
		}
	} while (1);
	close(ucache_dirfd);

	char *prefix_dirname = NULL;
	if (asprintf(&prefix_dirname, "%s/%s", ucache_dirname, srund_dirname) == -1) {
		neb_syslog(LOG_ERR, "asprintf: %m");
		close(cache_dirfd);
		return -1;
	}

	int ret = io_cache_open_default(sm, cache_dirfd, prefix_dirname, se);
	free(prefix_dirname);
	close(cache_dirfd);
	return ret;
}

int sr_io_sys_open_default(const sr_shmsg_t *sm, sr_exec_t *se)
{
	int cache_dirfd = neb_dir_open(srund_cache_dir, NULL);
	if (cache_dirfd < 0) {
		neb_syslog(LOG_ERR, "Failed to open sys cache dir %s", srund_cache_dir);
		return -1;
	}

	int ret = io_cache_open_default(sm, cache_dirfd, srund_cache_dir, se);
	close(cache_dirfd);
	return ret;
}

int sr_io_do_redirect(sr_exec_t *se)
{
	if (se->pty_index >= 0) {
		int pty_slave = se->io[se->pty_index].fd;
		if (neb_pty_login_tty(pty_slave) == -1) {
			neb_syslog(LOG_ERR, "Failed to set login tty");
			return -1;
		}
		if (neb_io_redirect_pty(pty_slave) != 0) {
			neb_syslog(LOG_ERR, "Failed to redirect io to terminal %d", pty_slave);
			return -1;
		}
		close(pty_slave);
		se->io[se->pty_index].fd = -1;
		return 0;
	} else if (se->dft_index >= 0) {
		int fd = se->io[se->dft_index].fd;
		if (neb_io_redirect_stdout(fd) != 0) {
			neb_syslog(LOG_ERR, "Failed to redirect stdout to dft fd %d", fd);
			return -1;
		}
		if (neb_io_redirect_stderr(fd) != 0) {
			neb_syslog(LOG_ERR, "Failed to redirect stderr to dft fd %d", fd);
			return -1;
		}
		close(fd);
		se->io[se->dft_index].fd = -1;
		return 0;
	}

	int has_stdout = 0;
	int has_stderr = 0;
	for (int i = 0; i < SRUN_MSG_MAX_FDS; i++) {
		int err = 0;
		int fd = se->io[i].fd;
		switch (se->io[i].type) {
		case SRUN_MSG_IO_TYPE_NONE:
			goto check_null_io;
			break;
		case SRUN_MSG_IO_TYPE_STDIN:
			err = neb_io_redirect_stdin(fd);
			break;
		case SRUN_MSG_IO_TYPE_STDERR:
			err = neb_io_redirect_stderr(fd);
			has_stderr = 1;
			break;
		case SRUN_MSG_IO_TYPE_STDOUT:
			err = neb_io_redirect_stdout(fd);
			has_stdout = 1;
			break;
		default:
			break;
		}

		if (err) {
			neb_syslog(LOG_ERR, "Failed to redirect io %d to %s", fd, io_type_str(se->io[i].type));
			return -1;
		}

		close(fd);
		se->io[i].fd = -1;
	}

check_null_io:
	if (!has_stderr) {
		if (neb_io_redirect_stderr(-1) != 0) {
			neb_syslog(LOG_ERR, "Failed to redirect stderr to null");
			return -1;
		}
	}
	if (!has_stdout) {
		if (neb_io_redirect_stdout(-1) != 0) {
			neb_syslog(LOG_ERR, "Failed to redirect stdout to null");
			return -1;
		}
	}
	return 0;
}
