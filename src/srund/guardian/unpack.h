
#ifndef SR_GUD_UNPACK_H
#define SR_GUD_UNPACK_H 1

#include <nebase/cdefs.h>

#include <srun/message.h>

#include <sys/types.h>

#include <uuid.h>

struct sr_io {
	int type;
	int fd;
};

enum {
	SR_WAIT_TYPE_NONE = 0,
	SR_WAIT_TYPE_SIGNAL,
	SR_WAIT_TYPE_FDIN,
	SR_WAIT_TYPE_FDHUP,
};

typedef struct {
	int orphan;

	int argc;
	char **argv;
	char *filename;
	char **envp;

	struct sr_io io[SRUN_MSG_MAX_FDS];
	int io_count;
	int dft_index;
	int pty_index;

	struct {
		int set;
		int type;
		int value;
		int timeout_ms;
	} wait;

	struct {
		int set;
		uuid_t id;
		int num;        // should not be to much
		int timeout_ms; // <0 to wait forever, =0 to skip
	} concurrency;

	struct {
		uid_t uid;
		gid_t gid;
	} cred;

	struct {
		int set;
		int policy;
		int priority;
	} sched;

	struct {
		int set;
	} rlimit;

	union {
		struct {
			int set;
		} priv;
		struct {
			int set;
		} cap;
	};
} sr_exec_t;

extern uid_t sr_cred_uid_default;
extern gid_t sr_cred_gid_default;

extern sr_exec_t *sr_gud_msg_unpack(const char *data, size_t len, int *fds, int fd_num)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void sr_exec_t_close_io(sr_exec_t *se)
	_nattr_nonnull((1));
extern void sr_exec_t_free(sr_exec_t *se)
	_nattr_nonnull((1));

#endif
