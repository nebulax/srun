
#include <nebase/syslog.h>
#include <nebase/signal.h>

#include "signal.h"

#include <stddef.h>
#include <signal.h>

int sr_gud_signal_block_all(void)
{
	sigset_t set;
	sigfillset(&set);
	if (sigprocmask(SIG_SETMASK, &set, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigprocmask(SIG_SETMASK): %m");
		return -1;
	}
	return 0;
}

int sr_gud_signal_unblock_all(void)
{
	sigset_t set;
	sigemptyset(&set);
	if (sigprocmask(SIG_SETMASK, &set, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigprocmask(SIG_SETMASK): %m");
		return -1;
	}
	return 0;
}

int se_gud_signal_unblock_handled(void)
{
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGCHLD);
	if (sigprocmask(SIG_UNBLOCK, &set, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigprocmask(SIG_UNBLOCK): %m");
		return -1;
	}
	return 0;
}

int sr_gud_signal_set_handler(void)
{
	struct sigaction act;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_handler = neb_sigterm_handler; // TODO use custom sigterm handler
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART | SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	act.sa_sigaction = neb_sigchld_action;
	if (sigaction(SIGCHLD, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	act.sa_handler = SIG_IGN; // Ignore SIGPIPE
	if (sigaction(SIGPIPE, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}
