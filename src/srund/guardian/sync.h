
#ifndef SR_GUD_SYNC_H
#define SR_GUD_SYNC_H 1

#include <nebase/cdefs.h>
#include <nebase/sem.h>

enum {
	SR_GUD_SEM_WAIT_PARENT = 0,
	SR_GUD_SEM_COUNT,
};

enum {
	SR_GUD_PREPARE_NOT_STARTED         =    0,

	SR_GUD_PREPARE_PREFORK_STARTED     = 1000,
	SR_GUD_PREPARE_PREFORK_FEXECVE     = 1001, // execve failed
	SR_GUD_PREPARE_PREFORK_FOPENPTY    = 1002, // openpty failed
	SR_GUD_PREPARE_PREFORK_FSUBREAPER  = 1003, // setup subreaper failed
	SR_GUD_PREPARE_PREFORK_FSEMCREATE  = 1004, // semaphore create failed
	SR_GUD_PREPARE_PREFORK_FSETSIGMASK = 1005, // set sigmask failed
	SR_GUD_PREPARE_PREFORK_FPIPECREATE = 1006, // pipe create failed
	SR_GUD_PREPARE_PREFORK_FFORK       = 1007, // fork failed

	SR_GUD_PREPARE_PARENT_STARTED      = 2000,
	SR_GUD_PREPARE_PARENT_FEVDPQUEUE   = 2001, // evdp queue create failed
	SR_GUD_PREPARE_PARENT_FEVDPCHILD   = 2002, // evdp child source create failed
	SR_GUD_PREPARE_PARENT_FEVDPPTY     = 2003, // evdp pty source create failed
	SR_GUD_PREPARE_PARENT_FSIGSETUP    = 2004, // setup signal handler failed
	SR_GUD_PREPARE_PARENT_FCHILDNOTIFY = 2005, // notify child failed
	SR_GUD_PREPARE_PARENT_FSIGUNBLOCK  = 2006, // unblock signal failed
	SR_GUD_PREPARE_PARENT_FCHILDSTATUS = 2007, // no child status
	SR_GUD_PREPARE_PARENT_FPIPEREAD    = 2008, // pipe read failed

	SR_GUD_PREPARE_CHILD_STARTED       = 3000,
	SR_GUD_PREPARE_CHILD_FWAITPARENT   = 3001, // wait parent failed
	SR_GUD_PREPARE_CHILD_FSETSID       = 3002, // setsid failed
	SR_GUD_PREPARE_CHILD_FIOREDIRECT   = 3003, // io redirect failed
	SR_GUD_PREPARE_CHILD_FSETCLOEXEC   = 3004, // set pipe to cloexec failed
	SR_GUD_PREPARE_CHILD_PREEXECVEPE   = 3900, // pre execvepe
	SR_GUD_PREPARE_CHILD_FNOTIFY       = 3901, // notify parent failed
	SR_GUD_PREPARE_CHILD_FCLRSIGMASK   = 3902, // clear sigmask failed
	SR_GUD_PREPARE_CHILD_FEXECVEPE     = 3903, // execvepe failed

	SR_GUD_PREPARE_ALL_DONE            = 4000,
};

extern int sr_gud_sem _nattr_hidden;
extern int sr_gud_sem_to_close _nattr_hidden;
extern int sr_gud_prepare_status _nattr_hidden;

#endif
