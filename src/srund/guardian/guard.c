
#include <nebase/syslog.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>
#include <nebase/events.h>

#include "guard.h"
#include "sync.h"
#include "signal.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define GUD_PREPARE_TIMEOUT_MS 2000

static neb_evdp_queue_t sr_gud_queue = NULL;
static neb_evdp_timer_t sr_gud_timer = NULL;

struct sr_gud_child_record {
	int already_in_free;

	neb_evdp_source_t pipe_s;
	void *pipe_s_timer;
};

static void sr_gud_child_record_free(struct sr_gud_child_record *cr)
{
	if (cr->already_in_free)
		return;
	cr->already_in_free = 1;
	if (cr->pipe_s_timer) {
		neb_evdp_timer_del_point(sr_gud_timer, cr->pipe_s_timer);
		cr->pipe_s_timer = NULL;
	}
	if (cr->pipe_s) {
		neb_evdp_queue_t q = neb_evdp_source_get_queue(cr->pipe_s);
		if (q) {
			if (neb_evdp_queue_detach(q, cr->pipe_s, 1) != 0)
				neb_syslog(LOG_ERR, "Failed to detach pipe_s %p from dq", cr->pipe_s);
		}
		neb_evdp_source_del(cr->pipe_s);
	}
	free(cr);
}

static neb_evdp_cb_ret_t on_child_monitor_event(int fd, void *udata _nattr_unused, const void *context _nattr_unused)
{
	int prepare_status;
	ssize_t nr = read(fd, &prepare_status, sizeof(prepare_status));
	switch (nr) {
	case -1:
		neb_syslog(LOG_ERR, "read: %m");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FPIPEREAD;
		return NEB_EVDP_CB_BREAK_ERR;
		break;
	case 0:
		break;
	default:
		if ((size_t)nr != sizeof(prepare_status)) {
			neb_syslog(LOG_ERR, "Invalid data (%ld bytes) received from pipe_s", nr);
			sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FPIPEREAD;
			return NEB_EVDP_CB_BREAK_ERR;
		}
		sr_gud_prepare_status = prepare_status;
		break;
	}

	return NEB_EVDP_CB_CONTINUE;
}

static neb_evdp_cb_ret_t on_child_monitor_closed(int fd _nattr_unused, void *udata _nattr_unused, const void *context _nattr_unused)
{
	// the last status shoud be SR_GUD_PREPARE_CHILD_PREEXECVEPE if everything is ok
	if (sr_gud_prepare_status != SR_GUD_PREPARE_CHILD_PREEXECVEPE) {
		return NEB_EVDP_CB_BREAK_EXP;
	} else {
		sr_gud_prepare_status = SR_GUD_PREPARE_ALL_DONE;
		return NEB_EVDP_CB_REMOVE;
	}
}

static int clean_pipe_s(neb_evdp_source_t pipe_s)
{
	struct sr_gud_child_record *cr = neb_evdp_source_get_udata(pipe_s);
	sr_gud_child_record_free(cr);
	return 0;
}

static neb_evdp_timeout_ret_t on_child_monitor_timeout(void *udata)
{
	struct sr_gud_child_record *cr = udata;
	sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FCHILDSTATUS;
	thread_events |= T_E_QUIT;
	cr->pipe_s_timer = NULL;
	sr_gud_child_record_free(cr);
	return NEB_EVDP_TIMEOUT_FREE;
}

static int add_child_prepare_monitor(int pipefd, const sr_exec_t *se)
{
	struct sr_gud_child_record *cr = calloc(1, sizeof(struct sr_gud_child_record));
	if (!cr) {
		neb_syslog(LOG_ERR, "calloc: %m");
		return -1;
	}

	neb_evdp_source_t pipe_s = neb_evdp_source_new_ro_fd(pipefd, on_child_monitor_event, on_child_monitor_closed);
	if (!pipe_s) {
		neb_syslog(LOG_ERR, "Failed to get child monitor pipe_s");
		sr_gud_child_record_free(cr);
		return -1;
	}
	cr->pipe_s = pipe_s;
	neb_evdp_source_set_udata(pipe_s, cr);
	neb_evdp_source_set_on_remove(pipe_s, clean_pipe_s);
	if (neb_evdp_queue_attach(sr_gud_queue, pipe_s) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach child monitor pipe_s to dq");
		sr_gud_child_record_free(cr);
		return -1;
	}

	int timeout_ms = GUD_PREPARE_TIMEOUT_MS;
	if (se->wait.set)
		timeout_ms += se->wait.timeout_ms;
	if (se->concurrency.set)
		timeout_ms += se->concurrency.timeout_ms;
	int64_t abs_timeout_ms = neb_evdp_queue_get_abs_timeout(sr_gud_queue, timeout_ms);
	cr->pipe_s_timer = neb_evdp_timer_new_point(sr_gud_timer, abs_timeout_ms, on_child_monitor_timeout, cr);
	if (!cr->pipe_s_timer) {
		neb_syslog(LOG_ERR, "Failed to get child monitor timer");
		sr_gud_child_record_free(cr);
		return -1;
	}

	return 0;
}

static int setup_pty_relay(int pty_remote, int pty_master)
{
	// TODO
	return 0;
}

static int let_child_do_prepare(pid_t cpid)
{
	if (neb_sem_proc_post(sr_gud_sem, SR_GUD_SEM_WAIT_PARENT) != 0) {
		neb_syslog(LOG_ERR, "Failed to tell child %d we are ready", cpid);
		return -1;
	}
	struct timespec ts = {.tv_sec = 1};
	if (neb_sem_proc_wait_zeroed(sr_gud_sem, SR_GUD_SEM_WAIT_PARENT, &ts) != 0) {
		neb_syslog(LOG_ERR, "Failed to get ready response from child %d", cpid);
		return -1;
	}
	neb_sem_proc_destroy(sr_gud_sem);
	sr_gud_sem_to_close = 0;
	return 0;
}

static neb_evdp_cb_ret_t thread_event_handler(void *udata _nattr_unused)
{
	if (thread_events & T_E_QUIT)
		return NEB_EVDP_CB_BREAK_EXP;
	if (thread_events & T_E_CHLD) {
		for (;;) { // TODO set a max batch size
			int wstatus = 0;
			pid_t cpid = waitpid(-1, &wstatus, WNOHANG);
			switch (cpid) {
			case -1:
				if (errno == ECHILD) {
					thread_events ^= T_E_CHLD;
					return NEB_EVDP_CB_BREAK_EXP;
				}
				neb_syslog(LOG_ERR, "waitpid: %m");
				return NEB_EVDP_CB_BREAK_ERR;
				break;
			case 0:
				thread_events ^= T_E_CHLD;
				break;
			default:
				if (WIFSIGNALED(wstatus)) {
					int sig = WTERMSIG(wstatus);
					neb_syslog(LOG_NOTICE, "Child process %d has been killed by signal %d", cpid, sig);
				}
				if (WIFEXITED(wstatus)) {
					int ret = WEXITSTATUS(wstatus);
					neb_syslog(LOG_INFO, "Child process %d exited with status %d", cpid, ret);
				}
				// TODO parse wstatus and set task status for main child
				break;
			}
		}
	}
	return NEB_EVDP_CB_CONTINUE;
}

int sr_gud_guard_run(sr_exec_t *se, const struct sr_gud_extra_param *sgep)
{
	sr_gud_queue = neb_evdp_queue_create(0);
	if (!sr_gud_queue) {
		neb_syslog(LOG_ERR, "Failed to get evdp queue");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FEVDPQUEUE;
		goto setup_error;
	}
	sr_gud_timer = neb_evdp_timer_create(10, 10); // 10 cache node is enough
	if (!sr_gud_timer) {
		neb_syslog(LOG_ERR, "Failed to get evdp timer");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FEVDPQUEUE;
		goto setup_error;
	}
	neb_evdp_queue_set_timer(sr_gud_queue, sr_gud_timer);

	if (add_child_prepare_monitor(sgep->sync_pipe, se) != 0) {
		neb_syslog(LOG_ERR, "Failed to add child prepare monitor");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FEVDPCHILD;
		goto setup_error;
	}

	if (sgep->pty_remote >= 0) {
		if (setup_pty_relay(sgep->pty_remote, sgep->pty_master) != 0) {
			neb_syslog(LOG_ERR, "Failed to setup pty relay");
			sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FEVDPPTY;
			goto setup_error;
		}
	}

	if (sr_gud_signal_set_handler() != 0) {
		neb_syslog(LOG_ERR, "Failed to setup sigal handler");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FSIGSETUP;
		goto setup_error;
	}

	// tell child to continue
	if (let_child_do_prepare(sgep->cpid) != 0) {
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FCHILDNOTIFY;
		goto wait_continue_error;
	}

	if (se_gud_signal_unblock_handled() != 0) {
		neb_syslog(LOG_ERR, "Failed to unblock signals");
		sr_gud_prepare_status = SR_GUD_PREPARE_PARENT_FSIGUNBLOCK;
		goto setup_error;
	}

	sr_gud_prepare_status = SR_GUD_PREPARE_CHILD_STARTED;
	neb_evdp_queue_set_event_handler(sr_gud_queue, thread_event_handler);
	if (neb_evdp_queue_run(sr_gud_queue) != 0)
		neb_syslog(LOG_ERR, "Error occure while running evdp_queue");

	// TODO handle child
	// TODO handle db
	return 0;

wait_continue_error:
	// TODO
setup_error:
	if (sr_gud_queue) {
		neb_evdp_queue_destroy(sr_gud_queue);
		sr_gud_queue = NULL;
	}
	// TODO handle child
	// TODO handle db
	return -1;
}
