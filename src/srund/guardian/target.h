
#ifndef SR_GUD_TARGET_H
#define SR_GUD_TARGET_H 1

#include <nebase/cdefs.h>

#include "unpack.h"

extern int sr_gud_target_run(sr_exec_t *se, int notify_pipe, int unprivileged)
	_nattr_warn_unused_result _nattr_nonnull((1));

#endif
