
#ifndef SRUN_HJK_SIGNAL_H
#define SRUN_HJK_SIGNAL_H 1

#include <nebase/cdefs.h>

#include <signal.h>

extern volatile sig_atomic_t run_offline_mode;

extern void sr_hjk_signal_block(void);
extern void sr_hjk_signal_unblock_main(void);
extern void sr_hjk_signal_unblock_labor(void);

extern int sr_hjk_signal_handler_set_main(void)
	_nattr_warn_unused_result;
extern int sr_hjk_signal_handler_set_labor(void)
	_nattr_warn_unused_result;

#endif
