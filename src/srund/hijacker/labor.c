
#include <nebase/syslog.h>
#include <nebase/thread.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>
#include <nebase/events.h>
#include <nebase/pipe.h>

#include "labor.h"
#include "signal.h"
#include "relay.h"

#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/uio.h>

typedef struct labor {
	pthread_t ptid;
	int pipe[2];
	int num;
	int created;
	_Atomic int listen_hup;
	_Atomic int job_count;
} labor_t;

struct pipe_ds_udata {
	neb_evdp_queue_t dq;
	int error;
	int removed;
};

static int labor_num = 0;
static labor_t *labors = NULL;
static _Thread_local labor_t *labor_p = NULL;

static int labor_rr_last = 0;

static neb_evdp_cb_ret_t read_handler(int fd, void *user_data, const void *context _nattr_unused)
{
	struct pipe_ds_udata *u = user_data;
	sr_hjk_msg_t msg;
	int fds[2] = {[0] = -1, [1] = -1};
	struct iovec iov[] = {
		[0] = {
			.iov_base = &msg,
			.iov_len = sizeof(msg)
		},
		[1] = {
			.iov_base = fds,
			.iov_len = sizeof(fds)
		}
	};
	ssize_t nr = readv(fd, iov, 2);
	if (nr == -1) {
		neb_syslog(LOG_ERR, "read: %m");
		u->error = 1;
		return NEB_EVDP_CB_BREAK_ERR;
	}
	if (nr == 0)
		return NEB_EVDP_CB_CONTINUE;
	int pipe_rfd = fds[0];
	int file_wfd = fds[1];
	if (pipe_rfd < 0 || file_wfd < 0) {
		neb_syslog(LOG_ERR, "Invalid fds: pipe_rfd: %d, file_wfd: %d", pipe_rfd, file_wfd);
		if (pipe_rfd >= 0)
			close(pipe_rfd);
		if (file_wfd >= 0)
			close(file_wfd);
		return NEB_EVDP_CB_CONTINUE;
	}
	sr_hjk_relay_handle(&msg, pipe_rfd, file_wfd, u->dq);
	return NEB_EVDP_CB_CONTINUE;
}

static neb_evdp_cb_ret_t hup_handler(int fd, void *user_data, const void *context)
{
	struct pipe_ds_udata *u = user_data;
	if (run_offline_mode) {
		labor_p->listen_hup = 1;
		u->removed = 1;
		return NEB_EVDP_CB_REMOVE;
	} else {
		int sockerr;
		if (neb_evdp_sock_get_sockerr(context, &sockerr) != 0) {
			neb_syslog(LOG_CRIT, "Listen fd %d hup, but we failed to get the error code", fd);
		} else {
			neb_syslog_en(sockerr, LOG_CRIT, "Listen fd %d hup: %m", fd);
		}
		u->error = 1;
		return NEB_EVDP_CB_BREAK_ERR;
	}
}

static neb_evdp_cb_ret_t thread_events_handler(void *user_data _nattr_unused)
{
	if (thread_events & T_E_QUIT) {
		if (run_offline_mode) {
			thread_events ^= T_E_QUIT;
			return NEB_EVDP_CB_CONTINUE;
		} else {
			return NEB_EVDP_CB_BREAK_EXP;
		}
	}

	return NEB_EVDP_CB_CONTINUE;
}

static void clean_pipe(int fd)
{
	for (;;) {
		sr_hjk_msg_t msg;
		int fds[2] = {[0] = -1, [1] = -1};
		struct iovec iov[] = {
			[0] = {
				.iov_base = &msg,
				.iov_len = sizeof(msg)
			},
			[1] = {
				.iov_base = fds,
				.iov_len = sizeof(fds)
			}
		};
		ssize_t nr = readv(fd, iov, 2);
		if (nr != (ssize_t)(sizeof(fds) + sizeof(msg)))
			break;
		if (fds[0] >= 0)
			close(fds[0]);
		if (fds[1] >= 0)
			close(fds[1]);
	}
}

static void *labor_exec(void *arg)
{
	labor_t *labor = arg;
	labor_p = labor;
	int rfd = labor->pipe[0];

	if (neb_thread_register() != 0) {
		neb_syslog(LOG_ERR, "Failed to register");
		goto exit_quit;
	}

	if (sr_hjk_signal_handler_set_labor() != 0) {
		neb_syslog(LOG_ERR, "Failed to set signal handler");
		goto exit_quit;
	}

	neb_evdp_queue_t dq = neb_evdp_queue_create(0);
	if (!dq) {
		neb_syslog(LOG_ERR, "Failed to create evdp queue");
		goto exit_quit;
	}

	struct pipe_ds_udata pipe_udata = {
		.dq = dq,
		.error = 0,
		.removed = 0
	};
	neb_evdp_source_t pipe_ds = neb_evdp_source_new_ro_fd(rfd, read_handler, hup_handler);
	if (!pipe_ds) {
		neb_syslog(LOG_ERR, "Failed to create pipe listen evdp source");
		goto exit_destroy_dq;
	}
	neb_evdp_source_set_udata(pipe_ds, &pipe_udata);
	if (neb_evdp_queue_attach(dq, pipe_ds) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach pipe_ds to dq");
		goto exit_del_pipe_ds;
	}

	if (neb_thread_set_ready() != 0) {
		neb_syslog(LOG_ERR, "Failed to notify ready");
		goto exit_rm_pipe_ds;
	}

	sr_hjk_signal_unblock_labor();

	neb_evdp_queue_set_event_handler(dq, thread_events_handler);
	if (neb_evdp_queue_run(dq) != 0)
		neb_syslog(LOG_ERR, "Error occured while running dq");

exit_rm_pipe_ds:
	if (!pipe_udata.removed) {
		if (neb_evdp_queue_detach(dq, pipe_ds, 0) != 0)
			neb_syslog(LOG_ERR, "Failed to detach pipe_ds from dq");
	}
exit_del_pipe_ds:
	neb_evdp_source_del(pipe_ds);
exit_destroy_dq:
	neb_evdp_queue_destroy(dq);
exit_quit:
	neb_syslog(LOG_INFO, "Labor %d quit", labor->num);
	clean_pipe(rfd);
	pthread_exit(NULL);
}

int sr_hjk_labor_create(void)
{
	labor_num = sysconf(_SC_NPROCESSORS_ONLN);
	if (labor_num <= 0) {
		neb_syslog(LOG_ERR, "sysconf: %m");
		return -1;
	}
	labors = malloc(sizeof(labor_t) * labor_num);
	if (!labors) {
		neb_syslog(LOG_ERR, "malloc: %m");
		return -1;
	}
	// quick init
	for (int i = 0; i < labor_num; i++) {
		labor_t *labor = &labors[i];
		labor->num = i;
		labor->pipe[0] = -1;
		labor->pipe[1] = -1;
		labor->created = 0;
		labor->job_count = 0;
		labor->listen_hup = 0;
	}
	// real init
	for (int i = 0; i < labor_num; i++) {
		labor_t *labor = &labors[i];
		if (neb_pipe_new(labor->pipe) == -1) {
			neb_syslog(LOG_ERR, "Failed to create pipe");
			sr_hjk_labor_destroy();
			return -1;
		}

		if (neb_thread_create(&labor->ptid, NULL, labor_exec, labor) != 0) {
			neb_syslog(LOG_ERR, "labor %d create failed", labor->num);
			sr_hjk_labor_destroy();
			return -1;
		}
		labor->created = 1;
	}
	return 0;
}

void sr_hjk_labor_wait_done(void)
{
	if (!labors)
		return;

	// TODO set title

	for (;;) {
		if (!run_offline_mode)
			break;

		// close the pipe write end
		for (int i = 0; i < labor_num; i++) {
			labor_t *labor = &labors[i];
			if (labor->pipe[1] >= 0) {
				close(labor->pipe[1]);
				labor->pipe[1] = -1;
			}
		}

		int not_done = 0;
		for (int i = 0; i < labor_num; i++) {
			labor_t *labor = &labors[i];
			if (!labor->listen_hup || labor->job_count) {
				not_done = 1;
				break;
			}
		}

		if (!not_done)
			break;
		sleep(1);
	}
}

void sr_hjk_labor_destroy(void)
{
	if (!labors)
		return;

	for (int i = 0; i < labor_num; i++) {
		labor_t *labor = &labors[i];
		if (labor->created) {
			void *retval = NULL;
			if (neb_thread_destroy(labor->ptid, SIGUSR1, &retval) != 0)
				neb_syslog(LOG_ERR, "Failed to destroy labor %d thread", labor->num);
		}

		if (labor->pipe[0] >= 0) {
			close(labor->pipe[0]);
			labor->pipe[0] = -1;
		}
		if (labor->pipe[1] >= 0) {
			close(labor->pipe[1]);
			labor->pipe[1] = -1;
		}
	}

	free(labors);
	labors = NULL;
	labor_num = 0;
}

int sr_hjk_labor_check(void)
{
	if (!labors) {
		neb_syslog(LOG_ERR, "No labors inited");
		return -1;
	}
	for (int i = 0; i < labor_num; i++) {
		labor_t *labor = &labors[i];
		if (!labor->created) {
			neb_syslog(LOG_ERR, "Labor %d is not started", labor->num);
			return -1;
		}
		if (!neb_thread_is_running(labor->ptid)) {
			neb_syslog(LOG_ERR, "Labor %d is not running", labor->num);
			return -1;
		}
	}
	return 0;
}

int sr_hjk_labor_send(const sr_hjk_msg_t *msg, int pipe_rfd, int file_wfd)
{
	if (labor_rr_last >= labor_num)
		labor_rr_last = 0;
	labor_t *labor = labors + labor_rr_last++;
	int wfd = labor->pipe[1];
	int fds[] = {
		[0] = pipe_rfd,
		[1] = file_wfd
	};
	struct iovec iov[] = {
		[0] = {
			.iov_base = (char *)msg,
			.iov_len = sizeof(*msg)
		},
		[1] = {
			.iov_base = fds,
			.iov_len = sizeof(fds)
		}
	};
	ssize_t nw = writev(wfd, iov, 2);
	if (nw == -1) {
		neb_syslog(LOG_ERR, "write: %m");
		return -1;
	}
	return 0;
}

void sr_hjk_labor_inc_job(void)
{
	labor_p->job_count++;
}

void sr_hjk_labor_dec_job(void)
{
	labor_p->job_count--;
}
