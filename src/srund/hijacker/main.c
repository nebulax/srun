
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/sock/unix.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>
#include <nebase/evdp/sys_timer.h>
#include <nebase/events.h>
#include <nebase/thread.h>

#include <src/srund/proc.h>

#include "main.h"
#include "signal.h"
#include "labor.h"
#include "msg.h"

#include <unistd.h>
#include <sys/stat.h>

#define TIMER_LABOR_CHECK 0

const char *sr_hjk_listen_path = SRUND_HIJACK_SOCKET;

static neb_evdp_cb_ret_t labor_check(unsigned int timer_id _nattr_unused, long overrun _nattr_unused, void *data)
{
	int *labor_error = data;
	if (sr_hjk_labor_check() != 0) {
		*labor_error = 1;
		return NEB_EVDP_CB_BREAK_ERR;
	} else {
		return NEB_EVDP_CB_CONTINUE;
	}
}

static neb_evdp_cb_ret_t read_handler(int fd, void *user_data, const void *context _nattr_unused)
{
	int ret = NEB_EVDP_CB_CONTINUE;
	sr_hjk_msg_t msg;
	int fds[2];
	int *pipe_fdp = &fds[0], *file_fdp = &fds[1];
	int fd_num = SR_HJK_MSG_FD_NUM;
	*pipe_fdp = -1;
	*file_fdp = -1;
	int nr = neb_sock_unix_recv_with_fds(fd, (char *)&msg, sizeof(msg), fds, &fd_num);
	if (nr == -1) {
		neb_syslog(LOG_ERR, "Failed to recv hjk msg");
		int *listen_err = (int *)user_data;
		*listen_err = 1;
		ret = NEB_EVDP_CB_BREAK_ERR;
		goto exit_err;
	} else if (nr != (int)sizeof(msg)) {
		neb_syslog(LOG_ERR, "Invalid hjk msg, received msg size %d", nr);
		goto exit_err;
	} else if (fd_num != SR_HJK_MSG_FD_NUM) {
		neb_syslog(LOG_ERR, "Invalid hjk msg, fd_num: %d, which should be %d", fd_num, SR_HJK_MSG_FD_NUM);
		goto exit_err;
	}

	if (sr_hjk_labor_send(&msg, *pipe_fdp, *file_fdp) != 0) {
		neb_syslog(LOG_ERR, "Failed to tell labor new io fds");
		goto exit_err;
	}

	return NEB_EVDP_CB_CONTINUE;

exit_err:
	if (*pipe_fdp >= 0)
		close(*pipe_fdp);
	if (*file_fdp >= 0)
		close(*file_fdp);
	return ret;
}

static neb_evdp_cb_ret_t hup_handler(int fd, void *user_data, const void *context)
{
	int sockerr;
	if (neb_evdp_sock_get_sockerr(context, &sockerr) != 0) {
		neb_syslog(LOG_CRIT, "Listen fd %d hup, but we failed to get the error code", fd);
	} else {
		neb_syslog_en(sockerr, LOG_CRIT, "Listen fd %d hup: %m", fd);
	}
	int *listen_err = (int *)user_data;
	*listen_err = 1;
	return NEB_EVDP_CB_BREAK_ERR;
}

static neb_evdp_cb_ret_t thread_events_handler(void *user_data _nattr_unused)
{
	if (thread_events & T_E_QUIT)
		return NEB_EVDP_CB_BREAK_EXP;

	return NEB_EVDP_CB_CONTINUE;
}

int sr_hjk_exec(void)
{
	int ret = -1;

	sr_hjk_signal_block();
	if (sr_hjk_signal_handler_set_main() != 0) {
		neb_syslog(LOG_ERR, "Failed to set signal handlers");
		goto exit_return;
	}

	if (neb_thread_init() != 0) {
		neb_syslog(LOG_ERR, "Failed to init thread");
		goto exit_return;
	}

	int fd = neb_sock_unix_new_binded(SOCK_DGRAM, sr_hjk_listen_path);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "Failed to bind to listen socket %s", sr_hjk_listen_path);
		goto exit_deinit_thread;
	}
	if (chmod(sr_hjk_listen_path, 0600) == -1) {
		neb_syslog(LOG_ERR, "chmod(%s): %m", sr_hjk_listen_path);
		goto exit_close_socket;
	}

	neb_evdp_queue_t dq = neb_evdp_queue_create(0);
	if (!dq) {
		neb_syslog(LOG_ERR, "Failed to create evdp queue");
		goto exit_close_socket;
	}

	int listen_err = 0;
	neb_evdp_source_t sock_ds = neb_evdp_source_new_ro_fd(fd, read_handler, hup_handler);
	if (!sock_ds) {
		neb_syslog(LOG_ERR, "Failed to create listen evdp source");
		goto exit_destroy_dq;
	}
	neb_evdp_source_set_udata(sock_ds, &listen_err);
	if (neb_evdp_queue_attach(dq, sock_ds) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach sock_ds to dq");
		goto exit_del_sock_ds;
	}

	int labor_error = 0;
	neb_evdp_source_t check_ds = neb_evdp_source_new_itimer_s(TIMER_LABOR_CHECK, 4, labor_check);
	if (!check_ds) {
		neb_syslog(LOG_ERR, "Failed to create labor check timer source");
		goto exit_rm_sock_ds;
	}
	neb_evdp_source_set_udata(check_ds, &labor_error);
	if (neb_evdp_queue_attach(dq, check_ds) != 0) {
		neb_syslog(LOG_ERR, "Failed to add check_ds to dq");
		goto exit_del_check_ds;
	}

	if (sr_hjk_labor_create() != 0) {
		neb_syslog(LOG_ERR, "Failed to create labors");
		goto exit_rm_check_ds;
	}

	sr_hjk_signal_unblock_main();

	if (sr_proc_sem_child_set_ready() != 0) {
		neb_syslog(LOG_ERR, "Failed to tell we are ready");
		goto exit_rm_check_ds;
	}

	neb_evdp_queue_set_event_handler(dq, thread_events_handler);
	if (neb_evdp_queue_run(dq) != 0) {
		neb_syslog(LOG_ERR, "Error occured while running dq");
	} else if (!listen_err && !labor_error) {
		ret = 0;
	}

exit_rm_check_ds:
	if (neb_evdp_queue_detach(dq, check_ds, 0) != 0)
		neb_syslog(LOG_ERR, "Failed to detach check_ds from dq");
exit_del_check_ds:
	neb_evdp_source_del(check_ds);
exit_rm_sock_ds:
	if (neb_evdp_queue_detach(dq, sock_ds, 0) != 0)
		neb_syslog(LOG_ERR, "Failed to detach sock_ds from dq");
exit_del_sock_ds:
	neb_evdp_source_del(sock_ds);
exit_destroy_dq:
	neb_evdp_queue_destroy(dq);
exit_close_socket:
	close(fd);
	unlink(sr_hjk_listen_path);
	// offline mode
	if (run_offline_mode) {
		neb_syslog(LOG_INFO, "Run into offline mode");
		sr_hjk_labor_wait_done();
		neb_syslog(LOG_INFO, "All labor done, quiting now");
		run_offline_mode = 0;
	}
	sr_hjk_labor_destroy();
exit_deinit_thread:
	neb_thread_deinit();
exit_return:
	return ret;
}

int sr_hjk_send_msg(sr_hjk_msg_t *m, int pipe_rfd, int file_fd)
{
	int ret = 0;

	int sock_fd = neb_sock_unix_new(SOCK_DGRAM);
	if (sock_fd < 0) {
		neb_syslog(LOG_ERR, "Failed to get new dgram fd");
		return -1;
	}
	struct sockaddr_un saddr;
	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, sr_hjk_listen_path, sizeof(saddr.sun_path) - 1);
	saddr.sun_path[sizeof(saddr.sun_path) - 1] = '\0';

	int fds[SR_HJK_MSG_FD_NUM] = {pipe_rfd, file_fd};
	int nw = neb_sock_unix_send_with_fds(sock_fd, (char *)m, sizeof(sr_hjk_msg_t), fds, SR_HJK_MSG_FD_NUM, (struct sockaddr *)&saddr, sizeof(saddr));
	if (nw != (int)sizeof(sr_hjk_msg_t)) {
		neb_syslog(LOG_ERR, "Failed to send hjk msg to hijacker");
		ret = -1;
	}

	close(sock_fd);
	return ret;
}

int sr_hjk_set_offline(pid_t pid)
{
	if (kill(pid, SIGINT) != 0) { // continue run in offline mode
		neb_syslog(LOG_ERR, "kill: %m");
		return -1;
	}
	return 0;
}
