
#ifndef SR_HJK_MSG_H
#define SR_HJK_MSG_H 1

#include <uuid.h>

enum {
	SR_HJK_IO_TYPE_NONE = 0,
	SR_HJK_IO_TYPE_DEFAULT,
	SR_HJK_IO_TYPE_STDOUT,
	SR_HJK_IO_TYPE_STDERR,
};

#define SR_HJK_MSG_FD_NUM 2

#define SR_HJK_MSG_LIMIT_DEFAULT (256 << 10) // 256KB

typedef struct sr_hjk_msg {
	int type;
	int limit;
	uuid_t id;
} sr_hjk_msg_t; /* with pipe_rfd & file_wfd */

#endif
