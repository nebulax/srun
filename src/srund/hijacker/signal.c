
#include <nebase/syslog.h>
#include <nebase/events.h>

#include "signal.h"

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

volatile sig_atomic_t run_offline_mode = 0;

void sr_hjk_signal_block()
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGQUIT);
	sigaddset(&set, SIGUSR1);

	ret = pthread_sigmask(SIG_BLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

void sr_hjk_signal_unblock_main()
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGQUIT);

	ret = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

void sr_hjk_signal_unblock_labor()
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);

	ret = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

static void main_sigterm_action(int sig _nattr_unused, siginfo_t *info, void *ucontext _nattr_unused)
{
	if (info->si_uid == 0 || info->si_pid == getppid()) {
		thread_events |= T_E_QUIT;
		run_offline_mode = 0;
	}
}

static void main_sigint_action(int sig _nattr_unused, siginfo_t *info, void *ucontext _nattr_unused)
{
	if (info->si_uid == 0 || info->si_pid == getppid()) {
		thread_events |= T_E_QUIT;
		run_offline_mode = 1;
	}
}

int sr_hjk_signal_handler_set_main(void)
{
	struct sigaction act;

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGTERM);
	act.sa_sigaction = main_sigint_action;
	if (sigaction(SIGINT, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	sigaddset(&act.sa_mask, SIGINT);
	act.sa_sigaction = main_sigterm_action;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	act.sa_handler = SIG_IGN; // Ignore SIGPIPE
	if (sigaction(SIGPIPE, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}

static void labor_sigusr1_action(int sig _nattr_unused, siginfo_t *info, void *ucontext _nattr_unused)
{
	if (info->si_uid == 0 || info->si_pid == getpid())
		thread_events |= T_E_QUIT;
}

int sr_hjk_signal_handler_set_labor(void)
{
	struct sigaction act;

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_sigaction = labor_sigusr1_action;
	if (sigaction(SIGUSR1, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	act.sa_handler = SIG_IGN; // Ignore SIGPIPE
	if (sigaction(SIGPIPE, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}
