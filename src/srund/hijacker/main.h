
#ifndef SRUN_HJK_MAIN_H
#define SRUN_HJK_MAIN_H 1

#include <nebase/cdefs.h>

#include "msg.h"

extern const char *sr_hjk_listen_path;
extern const char *sr_hjk_io_cache_dir;

extern int sr_hjk_exec(void)
	_nattr_warn_unused_result;

extern int sr_hjk_send_msg(sr_hjk_msg_t *m, int pipe_rfd, int file_fd)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern int sr_hjk_set_offline(pid_t pid)
	_nattr_warn_unused_result;

#endif
