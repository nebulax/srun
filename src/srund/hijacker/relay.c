
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>

#include "relay.h"
#include "labor.h"

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#define COPY_BLOCK_SIZE (16 << 10)  // 16KB
#define COPY_BATCH_SIZE (128 << 10) // 128KB

struct relay_record {
	int type;
	int pipe_fd;
	int file_fd;
	int limit;
	int offset;
	char uuid[UUID_STR_LEN + 1];
	neb_evdp_source_t pipe_s;
	neb_evdp_source_t file_s;
	int read_ready;
	int write_ready;
#if defined(OS_LINUX)
#else
	char *buf;
	int buf_size;
	int w_offset;
	int r_offset;
#endif
	int already_in_free;
};

static neb_evdp_cb_ret_t pipe_read_handler(int fd, void *user_data, const void *context);
static neb_evdp_cb_ret_t file_write_handler(int fd, void *user_data, const void *context);

static void relay_record_free(struct relay_record *rr)
{
	if (rr->already_in_free)
		return;
	rr->already_in_free = 1;
	if (rr->pipe_s) {
		neb_evdp_queue_t q = neb_evdp_source_get_queue(rr->pipe_s);
		if (q) {
			if (neb_evdp_queue_detach(q, rr->pipe_s, 1) != 0)
				neb_syslog(LOG_ERR, "Failed to detach pipe_s %p from dq", rr->pipe_s);
		}
		neb_evdp_source_del(rr->pipe_s);
	}
	if (rr->file_s) {
		neb_evdp_queue_t q = neb_evdp_source_get_queue(rr->file_s);
		if (q) {
			if (neb_evdp_queue_detach(q, rr->file_s, 1) != 0)
				neb_syslog(LOG_ERR, "Failed to detach file_s %p from dq", rr->file_s);
		}
		neb_evdp_source_del(rr->file_s);
	}
	if (rr->pipe_fd >= 0)
		close(rr->pipe_fd);
	if (rr->file_fd >= 0)
		close(rr->file_fd);
#if defined(OS_LINUX)
#else
	if (rr->buf)
		free(rr->buf);
#endif
	free(rr);
	sr_hjk_labor_dec_job();
}

static int do_relay(struct relay_record *rr)
{
#if defined(OS_LINUX)
	if (rr->limit <= rr->offset) {
		neb_syslog(LOG_ERR, "IO size overflow for task %s type %d", rr->uuid, rr->type);
		return -1;
	}
	size_t max_len = rr->limit - rr->offset;
	size_t copy_max = (max_len > COPY_BATCH_SIZE) ? COPY_BATCH_SIZE : max_len;
	ssize_t nw = splice(rr->pipe_fd, NULL, rr->file_fd, NULL, copy_max, SPLICE_F_NONBLOCK);
	switch (nw) {
	case -1:
		if (errno == EAGAIN) {
			if (rr->read_ready) {
				// TODO add a timer for write_ready
				rr->write_ready = 0;
			}
			return 0;
		}
		neb_syslog(LOG_ERR, "splice: %m");
		return -1;
		break;
	case 0:
		// 0 means end of input
		rr->read_ready = 0;
		return 0;
		break;
	default:
		rr->offset += nw;
		rr->read_ready = 0; // Only reset read
		break;
	}
#else
	int need_event = 0;
	size_t copied = 0;
	for (; !need_event;) {
		if (rr->limit <= rr->offset) {
			neb_syslog(LOG_ERR, "IO size overflow for task %s type %d", rr->uuid, rr->type);
			return -1;
		}
		size_t max_len = rr->limit - rr->offset;
		size_t copy_max = rr->buf_size - rr->r_offset;

		ssize_t to_read = copy_max < max_len ? copy_max : max_len;
		if (to_read > 0) {
			ssize_t nr = read(rr->pipe_fd, rr->buf + rr->r_offset, to_read);
			switch (nr) {
			case -1:
				if (errno == EAGAIN || errno == EWOULDBLOCK) {
					rr->read_ready = 0;
					return 0;
				}
				neb_syslog(LOG_ERR, "read: %m");
				return -1;
				break;
			case 0:
				// end of input
				rr->read_ready = 0;
				return 0;
				break;
			default:
				rr->r_offset += nr;
				if (nr < to_read) {
					rr->read_ready = 0;
					need_event = 1;
				}
				break;
			}
		}

		ssize_t to_write = rr->r_offset - rr->w_offset;
		if (to_write > 0) {
			ssize_t nw = write(rr->file_fd, buf + rr->w_offset, to_write);
			if (nw == -1) {
				if (errno == EAGAIN || errno == EWOULDBLOCK) {
					// TODO add a timer for write_ready
					rr->write_ready = 0;
					return 0;
				}
				neb_syslog(LOG_ERR, "write: %m");
				return -1;
			} else {
				rr->w_offset += nw;
				rr->offset += nw;
				copied += nw;
				if (nw < to_write) {
					// TODO add a timer for write_ready
					rr->write_ready = 0;
					need_event = 1;
				}
			}
		}

		if (rr->r_offset == rr->w_offset) {
			rr->r_offset = 0;
			rr->w_offset = 0;
		}

		if (copied >= COPY_BATCH_SIZE) {
			if (rr->write_ready && rr->read_ready)
				rr->read_ready = 0;
			return 0;
		}
	}
#endif

	return 0;
}

static neb_evdp_cb_ret_t retrigger_events(const struct relay_record *rr)
{
	if (!rr->write_ready && neb_evdp_source_os_fd_next_write(rr->file_s, file_write_handler) != 0) {
		neb_syslog(LOG_ERR, "Failed to enable write event on file_s %p", rr->file_s);
		return NEB_EVDP_CB_REMOVE;
	}
	if (!rr->read_ready && neb_evdp_source_os_fd_next_read(rr->pipe_s, pipe_read_handler) != 0) {
		neb_syslog(LOG_ERR, "Failed to enable read event on pipe_s %p", rr->pipe_s);
		return NEB_EVDP_CB_REMOVE;
	}
	return NEB_EVDP_CB_CONTINUE;
}

static neb_evdp_cb_ret_t pipe_read_handler(int fd _nattr_unused, void *user_data, const void *context _nattr_unused)
{
	struct relay_record *rr = user_data;
	rr->read_ready = 1;
	if (rr->write_ready) {
		if (do_relay(rr) != 0) {
			neb_syslog(LOG_ERR, "do_relay failed");
			return NEB_EVDP_CB_CLOSE;
		}
	}

	return retrigger_events(rr);
}

static neb_evdp_cb_ret_t file_write_handler(int fd _nattr_unused, void *user_data, const void *context _nattr_unused)
{
	struct relay_record *rr = user_data;
	rr->write_ready = 1;
	if (rr->read_ready) {
		if (do_relay(rr) != 0) {
			neb_syslog(LOG_ERR, "do_relay failed");
			return NEB_EVDP_CB_CLOSE;
		}
	}

	return retrigger_events(rr);
}

static neb_evdp_cb_ret_t hup_handler(int fd _nattr_unused, void *user_data _nattr_unused, const void *context _nattr_unused)
{
	return NEB_EVDP_CB_CLOSE;
}

static int clean_pipe_ds(neb_evdp_source_t ds)
{
	struct relay_record *rr = neb_evdp_source_get_udata(ds);
	relay_record_free(rr);
	return 0;
}

static int clean_file_ds(neb_evdp_source_t ds)
{
	struct relay_record *rr = neb_evdp_source_get_udata(ds);
	relay_record_free(rr);
	return 0;
}

void sr_hjk_relay_handle(const sr_hjk_msg_t *msg, int pipe_rfd, int file_wfd, neb_evdp_queue_t dq)
{
	struct relay_record *rr = calloc(1, sizeof(struct relay_record));
	if (!rr) {
		neb_syslog(LOG_ERR, "malloc: %m");
		close(pipe_rfd);
		close(file_wfd);
		return;
	}
	sr_hjk_labor_inc_job();
	rr->type = msg->type;
	rr->pipe_fd = pipe_rfd;
	rr->file_fd = file_wfd;
	rr->write_ready = 1;
	rr->limit = msg->limit;
	rr->offset = 0;
	uuid_unparse_lower(msg->id, rr->uuid);
	// TODO add more conf to udata from msg
#if defined(OS_LINUX)
#else
	rr->buf_size = COPY_BLOCK_SIZE;
	rr->buf = malloc(rr->buf_size);
	if (!rr->buf) {
		neb_syslog(LOG_ERR, "malloc: %m");
		relay_record_free(rr);
		return;
	}
#endif

	rr->pipe_s = neb_evdp_source_new_os_fd(rr->pipe_fd, hup_handler);
	if (!rr->pipe_s) {
		neb_syslog(LOG_ERR, "Failed to create pipe evdp source for %d", rr->pipe_fd);
		relay_record_free(rr);
		return;
	}
	if (neb_evdp_source_os_fd_next_read(rr->pipe_s, pipe_read_handler) != 0) {
		neb_syslog(LOG_ERR, "Failed to set pipe read handler");
		relay_record_free(rr);
		return;
	}
	neb_evdp_source_set_udata(rr->pipe_s, rr);
	neb_evdp_source_set_on_remove(rr->pipe_s, clean_pipe_ds);
	if (neb_evdp_queue_attach(dq, rr->pipe_s) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach pipe_ds to dq");
		clean_pipe_ds(rr->pipe_s);
		return;
	}

	rr->file_s = neb_evdp_source_new_os_fd(rr->file_fd, hup_handler);
	if (!rr->file_s) {
		neb_syslog(LOG_ERR, "Failed to create file evdp source for %d", rr->file_fd);
		relay_record_free(rr);
		return;
	}
	neb_evdp_source_set_udata(rr->file_s, rr);
	neb_evdp_source_set_on_remove(rr->file_s, clean_file_ds);
	if (neb_evdp_queue_attach(dq, rr->file_s) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach file_ds to dq");
		clean_file_ds(rr->file_s);
		return;
	}
}
