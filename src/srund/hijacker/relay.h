
#ifndef SR_HJK_RELAY_H
#define SR_HJK_RELAY_H 1

#include <nebase/cdefs.h>
#include <nebase/evdp/core.h>

#include "msg.h"

extern void sr_hjk_relay_handle(const sr_hjk_msg_t *msg, int pipe_rfd, int file_wfd, neb_evdp_queue_t dq)
	_nattr_nonnull((1, 4));

#endif
