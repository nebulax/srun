
#ifndef SR_HJK_LABOR_H
#define SR_HJK_LABOR_H 1

#include <nebase/cdefs.h>

extern int sr_hjk_labor_create(void)
	_nattr_warn_unused_result;
extern void sr_hjk_labor_wait_done(void);
extern void sr_hjk_labor_destroy(void);
extern int sr_hjk_labor_check(void)
	_nattr_warn_unused_result;

#include "msg.h"

extern int sr_hjk_labor_send(const sr_hjk_msg_t *msg, int pipe_rfd, int file_wfd)
	_nattr_warn_unused_result _nattr_nonnull((1));

extern void sr_hjk_labor_inc_job(void);
extern void sr_hjk_labor_dec_job(void);

#endif
