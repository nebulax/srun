
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/signal.h>

#include "main.h"
#include "signal.h"
#include "proc.h"

#include "hijacker/main.h"
#include "ancestor/main.h"
#include "janitor/main.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

const char *srund_cache_dir = SRUND_CACHE_DIR;

static pid_t hijacker_pid = 0;
static pid_t ancestor_pid = 0;
static pid_t janitor_pid = 0;
static pid_t daemon_pid = 0;

static void check_child_status(void)
{
	const char *child_name = "unknown";
	pid_t cpid = neb_sigchld_info._sifields._sichld.pid;
	if (cpid == ancestor_pid)
		child_name = "ancestor";
	else if (cpid == janitor_pid)
		child_name = "janitor";
	else if (cpid == hijacker_pid)
		child_name = "hijacker";

	int wstatus = neb_sigchld_info._sifields._sichld.wstatus;
	if (WIFEXITED(wstatus))
		neb_syslog(LOG_ERR, "CHLD: %s:%d has exited with code %d", child_name, cpid, WEXITSTATUS(wstatus));
	if (WIFSIGNALED(wstatus))
		neb_syslog(LOG_ERR, "CHLD: %s:%d has been killed by signal %d", child_name, cpid, WTERMSIG(wstatus));

	thread_events |= T_E_QUIT;
}

static int signal_handler_set(void)
{
	struct sigaction act;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_handler = neb_sigterm_handler;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_handler = neb_sigterm_handler;
	if (sigaction(SIGINT, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGCHLD);
	act.sa_sigaction = neb_sigchld_action;
	if (sigaction(SIGCLD, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}
	neb_sigchld_info._sifields._sichld.refresh = 1;

	return 0;
}

int main(int argc, char *argv[])
{
	int ret = 0;
	int destroy_proc_sem = 0;

	daemon_pid = getpid();

	sr_signal_block_all();

	if (signal_handler_set() != 0) {
		neb_syslog(LOG_ERR, "Failed to set signal handlers");
		ret = -1;
		goto quit_daemon;
	}

	if (sr_proc_sem_create() != 0) {
		ret = -1;
		goto quit_daemon;
	}
	destroy_proc_sem = 1;

	hijacker_pid = fork();
	if (hijacker_pid == -1) {
		neb_syslog(LOG_ERR, "fork: %m");
		ret = -1;
		goto quit_daemon;
	} else if (hijacker_pid == 0) {
		neb_syslog(LOG_INFO, "hijacker process started");
		sr_signal_unblock_child();
		ret = sr_hjk_exec();
		neb_syslog(LOG_INFO, "hijacker process exit with code %d", ret);
		goto quit_child;
	}

	int sv[2];
	if (socketpair(AF_UNIX, SOCK_SEQPACKET | SOCK_NONBLOCK | SOCK_CLOEXEC, 0, sv) == -1) {
		neb_syslog(LOG_ERR, "socketpair: %m");
		ret = -1;
		goto quit_daemon;
	}

	ancestor_pid = fork();
	if (ancestor_pid == -1) {
		neb_syslog(LOG_ERR, "fork: %m");
		ret = -1;
		goto quit_daemon;
	} else if (ancestor_pid == 0) {
		close(sv[1]);
		pid_t act_pid = getpid();
		/* Run ancestor in a new session which has no controlling terminal */
		pid_t sid = setsid();
		if (sid == -1) {
			neb_syslog(LOG_ERR, "setsid: %m");
			goto quit_child;
		}
		neb_syslog(LOG_INFO, "ancestor process started as session %d", sid);
		sr_signal_unblock_child();
		ret = sr_act_exec(sv[0]);
		if (act_pid == getpid())
			neb_syslog(LOG_INFO, "ancestor process exit with code %d", ret);
		goto quit_child;
	}
	close(sv[0]);

	if (sr_proc_sem_wait_child(2) != 0) {
		neb_syslog(LOG_ERR, "Either hijacker or ancestor process is not ready after timeout");
		ret = -1;
		goto quit_daemon;
	}

	janitor_pid = fork();
	if (janitor_pid == -1) {
		neb_syslog(LOG_ERR, "fork: %m");
		ret = -1;
		goto quit_daemon;
	} else if (janitor_pid == 0) {
		neb_syslog(LOG_INFO, "janitor process started");
		sr_signal_unblock_child();
		ret = sr_jnt_exec(sv[1]);
		neb_syslog(LOG_INFO, "janitor process exit with code %d", ret);
		goto quit_child;
	}
	close(sv[1]);

	if (sr_proc_sem_wait_child(1) != 0) {
		neb_syslog(LOG_ERR, "Janitor process is not ready after timeout");
		ret = -1;
		goto quit_daemon;
	}
	destroy_proc_sem = 0; // destroyed in janitor, before accepting new tasks

	sr_signal_unblock_daemon();

	for (;;) {
		if (thread_events) {
			if (thread_events & T_E_QUIT)
				break;

			if (thread_events & T_E_CHLD) {
				sr_signal_block_all();
				check_child_status();
				thread_events ^= T_E_CHLD;
				neb_sigchld_info._sifields._sichld.refresh = 1;
				sr_signal_unblock_daemon();
				continue;
			}
		}

		sleep(60);
	}

quit_daemon:
	if (janitor_pid && sr_proc_quit_wait(janitor_pid, "janitor") != 0)
		neb_syslog(LOG_ERR, "Error occur while terminating janitor process %d", janitor_pid);
	if (ancestor_pid && sr_proc_quit_wait(ancestor_pid, "ancestor") != 0)
		neb_syslog(LOG_ERR, "Error occur while terminating ancestor process %d", ancestor_pid);
	if (hijacker_pid && sr_hjk_set_offline(hijacker_pid) != 0)
		neb_syslog(LOG_ERR, "Error occur while set hijacker process %d offline", hijacker_pid);
	if (destroy_proc_sem)
		sr_proc_sem_destroy();
	neb_syslog(LOG_INFO, "Daemon exit with code %d", ret);
quit_child:
	return ret;
}
