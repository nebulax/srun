
#ifndef SRUN_JNT_TASK_H
#define SRUN_JNT_TASK_H 1

#include <nebase/cdefs.h>
#include <nebase/sock/unix.h>
#include <srun/protocol.h>

#include <uuid.h>

/*
 * NOTE sh should be the one in req, and in following functions
 *      it will be reused internally
 */

extern void sr_jnt_task_return_not_found(srup_header_t *sh, int clt_fd)
	_nattr_nonnull((1));
extern void sr_jnt_task_return_invalid_req(srup_header_t *sh, int clt_fd)
	_nattr_nonnull((1));
extern void sr_jnt_task_return_internal_err(srup_header_t *sh, int clt_fd)
	_nattr_nonnull((1));
extern void sr_jnt_task_return_continue(srup_header_t *sh, int clt_fd)
	_nattr_nonnull((1));
extern void sr_jnt_task_return_ok_added(srup_header_t *sh, int clt_fd)
	_nattr_nonnull((1));

extern void sr_jnt_task_update_time(const uuid_t uuid);
/**
 * \param[in] clt_fd error will be sent out
 */
extern int sr_jnt_task_time_valid(srup_header_t *sh, int clt_fd)
	_nattr_warn_unused_result _nattr_nonnull((1));

extern void sr_jnt_task_new(srup_header_t *sh, const struct neb_ucred *cred, int clt_fd, int act_fd)
	_nattr_nonnull((1, 2));
extern void sr_jnt_task_set(srup_header_t *sh, const struct neb_ucred *cred, int clt_fd)
	_nattr_nonnull((1, 2));
extern void sr_jnt_task_get(srup_header_t *sh, const struct neb_ucred *cred, int clt_fd)
	_nattr_nonnull((1, 2));
extern void sr_jnt_task_kill(srup_header_t *sh, const struct neb_ucred *cred, int clt_fd)
	_nattr_nonnull((1, 2));

#endif
