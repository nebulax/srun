
#ifndef SRUN_JNT_MAIN_H
#define SRUN_JNT_MAIN_H 1

#include <nebase/cdefs.h>

extern const char *sr_jnt_listen_path;
extern const char *sr_jnt_db_path;

extern int sr_jnt_exec(int act_fd)
	_nattr_warn_unused_result;

#endif
