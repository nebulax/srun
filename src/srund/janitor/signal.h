
#ifndef SRUN_JNT_SIGNAL_H
#define SRUN_JNT_SIGNAL_H 1

#include <nebase/cdefs.h>

extern void sr_jnt_signal_block(void);
extern void sr_jnt_signal_unblock(void);

extern int sr_jnt_signal_handler_set(void)
	_nattr_warn_unused_result;

#endif
