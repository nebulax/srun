
#ifndef SR_JNT_LABOR_H
#define SR_JNT_LABOR_H 1

#include <nebase/cdefs.h>

extern int sr_jnt_labor_create(int act_fd)
	_nattr_warn_unused_result;
extern void sr_jnt_labor_destroy(void);

extern int sr_jnt_labor_handle(int fd)
	_nattr_warn_unused_result;

#endif
