
#include <nebase/syslog.h>

#include "task.h"

#include <sys/types.h>
#include <sys/socket.h>

#include <uuid.h>

static _Atomic time_t db_time_min = 0;
static _Atomic time_t db_time_max = 0;

static void srup_send_without_payload(srup_header_t *sh, int clt_fd)
{
	sh->next_len = 0;

	ssize_t nw = send(clt_fd, sh, sizeof(*sh), MSG_DONTWAIT | MSG_NOSIGNAL);
	if (nw == -1) {
		neb_syslog(LOG_ERR, "send: %m");
		return;
	}
	if (nw != (ssize_t)sizeof(*sh))
		neb_syslog(LOG_ERR, "send: not all data sent");
}

void sr_jnt_task_return_not_found(srup_header_t *sh, int clt_fd)
{
	sh->type = SRUP_TYPE_RSP_ERR;
	sh->err_code = SRUP_ERR_NOTFOUND;
	srup_send_without_payload(sh, clt_fd);
}

void sr_jnt_task_return_invalid_req(srup_header_t *sh, int clt_fd)
{
	sh->type = SRUP_TYPE_RSP_ERR;
	sh->err_code = SRUP_ERR_INVALID;
	srup_send_without_payload(sh, clt_fd);
}

void sr_jnt_task_return_internal_err(srup_header_t *sh, int clt_fd)
{
	sh->type = SRUP_TYPE_RSP_ERR;
	sh->err_code = SRUP_ERR_INTERNAL;
	srup_send_without_payload(sh, clt_fd);
}

void sr_jnt_task_return_continue(srup_header_t *sh, int clt_fd)
{
	sh->type = SRUP_TYPE_RSP_CONTINUE;
	srup_send_without_payload(sh, clt_fd);
}

void sr_jnt_task_return_ok_added(srup_header_t *sh, int clt_fd)
{
	sh->type = SRUP_TYPE_RSP_OK;
	srup_send_without_payload(sh, clt_fd);
}

void sr_jnt_task_update_time(const uuid_t uuid)
{
	time_t task_time = uuid_time(uuid, NULL);
	if (task_time < db_time_min)
		db_time_min = task_time;
	if (task_time > db_time_max)
		db_time_max = task_time;
}

int sr_jnt_task_time_valid(srup_header_t *sh, int clt_fd)
{
	time_t task_time = uuid_time(sh->uuid, NULL);

	if (task_time < db_time_min || task_time > db_time_max) {
		sr_jnt_task_return_not_found(sh, clt_fd);
		return 0;
	}

	return 1;
}
