
#include <nebase/syslog.h>
#include <nebase/events.h>

#include "signal.h"

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

void sr_jnt_signal_block()
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);

	ret = pthread_sigmask(SIG_BLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

void sr_jnt_signal_unblock()
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);

	ret = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

static void sr_jnt_sigterm_action(int sig _nattr_unused, siginfo_t *info, void *ucontext _nattr_unused)
{
	if (info->si_uid == 0 || info->si_pid == getppid())
		thread_events |= T_E_QUIT;
}

int sr_jnt_signal_handler_set(void)
{
	struct sigaction act;

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_sigaction = sr_jnt_sigterm_action;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	act.sa_handler = SIG_IGN; // Ignore SIGPIPE
	if (sigaction(SIGPIPE, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}
