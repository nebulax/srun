
add_library(_srund_jnt OBJECT
  main.c
  signal.c
  labor.c
  procdb.c
  task.c
  task_new.c
  task_set.c
  task_get.c
  task_kill.c
)
