
#ifndef SR_JNT_PROCDB_H
#define SR_JNT_PROCDB_H 1

#include <nebase/cdefs.h>

extern int sr_jnt_procdb_init(const char *dbfile)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern void sr_jnt_procdb_deinit(void);

#endif
