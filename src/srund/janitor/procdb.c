
#include <nebase/syslog.h>
#include <nebase/time.h>

#include "procdb.h"

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#include <sqlite3.h>

#define PROC_TABLE "proc"

static sqlite3 *procdb = NULL;

static void sqlite_log_handler(void *data _nattr_unused, int ret, const char *msg)
{
	int prio;

	switch (ret) {
	case SQLITE_OK:
		prio = LOG_INFO;
		break;
	case SQLITE_NOTICE:
	case SQLITE_NOTICE_RECOVER_WAL:
	case SQLITE_NOTICE_RECOVER_ROLLBACK:
		prio = LOG_NOTICE;
		break;
	case SQLITE_WARNING:
#ifdef SQLITE_WARNING_AUTOINDEX //since sqlite version 3.8.0
	case SQLITE_WARNING_AUTOINDEX:
#endif
		prio = LOG_WARNING;
		break;
	default:
		prio = LOG_ERR;
		break;
	}

	neb_syslog(prio, "SQLITE: %d: %s", ret, msg);
}

static int init_sqlite_lib(void)
{
	int ret = sqlite3_config(SQLITE_CONFIG_SERIALIZED);
	if (ret != SQLITE_OK) {
		neb_syslog(LOG_ERR, "sqlite3_config: %d: failed to set SQLITE_CONFIG_SERIALIZED", ret);
		return -1;
	}

	ret = sqlite3_config(SQLITE_CONFIG_LOG, sqlite_log_handler, NULL);
	if (ret != SQLITE_OK) {
		neb_syslog(LOG_ERR, "sqlite3_config: %d: failed to set SQLITE_CONFIG_LOG", ret);
		return -1;
	}

	//sqlite3_initialize(); //Thread safe and auto-called, no need to call it here

	return 0;
}

static void deinit_sqlite_lib(void)
{
	int ret = sqlite3_shutdown();
	if (ret != SQLITE_OK)
		neb_syslog(LOG_ERR, "sqlite3_shutdown: %d: failed to shutdown sqlite3", ret);
}

static int create_proc_table(void)
{
	//TODO
	return 0;
}

/**
 * daily clean
 */
static int clean_obsolete_records(void)
{
	//TODO
	return 0;
}

static int open_sqlite_db(const char *dbfile)
{
	int ret = sqlite3_open(dbfile, &procdb);
	switch (ret) {
	case SQLITE_OK:
		break;
	case SQLITE_CORRUPT:
	case SQLITE_NOTADB:
		if (unlink(dbfile) == -1) {
			neb_syslog(LOG_ERR, "unlink(%s): %m", dbfile);
			return -1;
		}
		ret = sqlite3_open(dbfile, &procdb);
		if (ret != SQLITE_OK) {
			neb_syslog(LOG_ERR, "sqlite3_open(%s): %s", dbfile, sqlite3_errstr(ret));
			return -1;
		}
		break;
	default:
		neb_syslog(LOG_ERR, "sqlite3_open(%s): %s", dbfile, sqlite3_errstr(ret));
		return -1;
		break;
	}

	const char sql_set_locking[] = "PRAGMA locking_mode=EXCLUSIVE"; // to all databases
	if (sqlite3_exec(procdb, sql_set_locking, NULL, NULL, NULL) != SQLITE_OK)
		return -1;

	const char sql_set_temp_store[] = "PRAGMA temp_store=MEMORY";
	if (sqlite3_exec(procdb, sql_set_temp_store, NULL, NULL, NULL) != SQLITE_OK)
		return -1;

	const char sql_set_journal[] = "PRAGMA main.journal_mode=WAL"; // WAL appeared in 3.7.0
	if (sqlite3_exec(procdb, sql_set_journal, NULL, NULL, NULL) != SQLITE_OK)
		return -1;

	const char sql_set_synchronous[] = "PRAGMA main.synchronous=NORMAL";
	if (sqlite3_exec(procdb, sql_set_synchronous, NULL, NULL, NULL) != SQLITE_OK)
		return -1;

	if (create_proc_table() != 0) {
		neb_syslog(LOG_ERR, "Failed to create proc table %s", PROC_TABLE);
		return -1;
	}

	return clean_obsolete_records();
}

int sr_jnt_procdb_init(const char *dbfile)
{
	if (init_sqlite_lib() != 0) {
		neb_syslog(LOG_ERR, "Failed to init sqlite3");
		return -1;
	}

	if (open_sqlite_db(dbfile) != 0) {
		neb_syslog(LOG_ERR, "Failed to open sqlite3 db %s", dbfile);
		return -1;
	}

	return 0;
}

void sr_jnt_procdb_deinit(void)
{
	if (procdb) {
		sqlite3_close(procdb);
		procdb = NULL;
	}
	deinit_sqlite_lib();
}
