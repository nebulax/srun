
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/sock/unix.h>
#include <nebase/evdp/core.h>
#include <nebase/evdp/io_base.h>
#include <nebase/events.h>

#include <src/srund/proc.h>

#include "main.h"
#include "signal.h"
#include "procdb.h"
#include "labor.h"

#include <unistd.h>
#include <sys/stat.h>

const char *sr_jnt_listen_path = SRUND_LISTEN_SOCKET;
const char *sr_jnt_db_path = SRUND_DEFAULT_DBFILE;

static neb_evdp_cb_ret_t accept_handler(int fd, void *user_data, const void *context _nattr_unused)
{
	int clt_fd = accept4(fd, NULL, NULL, SOCK_NONBLOCK | SOCK_CLOEXEC);
	if (clt_fd == -1) {
		neb_syslog(LOG_ERR, "accept4: %m");
		int *listen_err = (int *)user_data;
		*listen_err = 1;
		return NEB_EVDP_CB_BREAK_ERR;
	}
	if (neb_sock_unix_enable_recv_cred(SOCK_SEQPACKET, clt_fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to enable recv of creds on peer socket %d", clt_fd);
		return NEB_EVDP_CB_BREAK_ERR;
	}
	if (sr_jnt_labor_handle(clt_fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to handle client request by fd %d", clt_fd);
		close(clt_fd);
	}
	return NEB_EVDP_CB_CONTINUE;
}

static neb_evdp_cb_ret_t hup_handler(int fd, void *user_data, const void *context)
{
	int sockerr = 0;
	if (neb_evdp_sock_get_sockerr(context, &sockerr) != 0) {
		neb_syslog(LOG_CRIT, "Listen fd %d hup, but we failed to get the error code", fd);
	} else {
		neb_syslog_en(sockerr, LOG_CRIT, "Listen fd %d hup: %m", fd);
	}
	int *listen_err = (int *)user_data;
	*listen_err = 1;
	return NEB_EVDP_CB_BREAK_ERR;
}

static neb_evdp_cb_ret_t thread_events_handler(void *user_data _nattr_unused)
{
	if (thread_events & T_E_QUIT)
		return NEB_EVDP_CB_BREAK_EXP;

	return NEB_EVDP_CB_CONTINUE;
}

int sr_jnt_exec(int act_fd)
{
	int ret = -1;

	sr_jnt_signal_block();
	if (sr_jnt_signal_handler_set() != 0) {
		neb_syslog(LOG_ERR, "Failed to set signal handlers");
		goto exit_close_actfd;
	}

	if (sr_jnt_procdb_init(sr_jnt_db_path) != 0) {
		neb_syslog(LOG_ERR, "Failed to init db %s", sr_jnt_db_path);
		goto exit_close_actfd;
	}

	if (sr_jnt_labor_create(act_fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to create labor threads");
		goto exit_deinit_db;
	}

	int fd = neb_sock_unix_new_binded(SOCK_SEQPACKET, sr_jnt_listen_path);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "Failed to bind to listen socket %s", sr_jnt_listen_path);
		goto exit_destroy_labor;
	}
	if (chmod(sr_jnt_listen_path, 0666) == -1) {
		neb_syslog(LOG_ERR, "chmod(%s): %m", sr_jnt_listen_path);
		goto exit_close_socket;
	}
	if (listen(fd, 10) == -1) { // TODO config backlog
		neb_syslog(LOG_ERR, "listen: %m");
		goto exit_close_socket;
	}
	if (neb_sock_unix_enable_recv_cred(SOCK_SEQPACKET, fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to enable recv of creds on listen socket %s", sr_jnt_listen_path);
		goto exit_close_socket;
	}

	neb_evdp_queue_t dq = neb_evdp_queue_create(0);
	if (!dq) {
		neb_syslog(LOG_ERR, "Failed to create evdp queue");
		goto exit_close_socket;
	}

	int *listen_err = 0;
	neb_evdp_source_t sock_ds = neb_evdp_source_new_ro_fd(fd, accept_handler, hup_handler);
	if (!sock_ds) {
		neb_syslog(LOG_ERR, "Failed to create listen evdp source");
		goto exit_destroy_dq;
	}
	neb_evdp_source_set_udata(sock_ds, &listen_err);
	if (neb_evdp_queue_attach(dq, sock_ds) != 0) {
		neb_syslog(LOG_ERR, "Failed to attach sock_ds to dq");
		goto exit_del_sock_ds;
	}

	sr_jnt_signal_unblock();

	if (sr_proc_sem_child_set_ready() != 0) {
		neb_syslog(LOG_ERR, "Failed to tell we are ready");
		goto exit_del_sock_ds;
	}
	if (sr_proc_sem_wait_daemon() != 0) {
		neb_syslog(LOG_ERR, "Failed to wait daemon to be ready");
		goto exit_del_sock_ds;
	}
	sr_proc_sem_destroy();

	neb_evdp_queue_set_event_handler(dq, thread_events_handler);
	if (neb_evdp_queue_run(dq) != 0) {
		neb_syslog(LOG_ERR, "Error occured while running dq");
	} else if (!listen_err) {
		ret = 0;
	}

	if (neb_evdp_queue_detach(dq, sock_ds, 0) != 0)
		neb_syslog(LOG_ERR, "Failed to detach sock_ds from dq");
exit_del_sock_ds:
	neb_evdp_source_del(sock_ds);
exit_destroy_dq:
	neb_evdp_queue_destroy(dq);
exit_close_socket:
	close(fd);
	unlink(sr_jnt_listen_path);
exit_destroy_labor:
	sr_jnt_labor_destroy();
exit_deinit_db:
	sr_jnt_procdb_deinit();
exit_close_actfd:
	close(act_fd);
	return ret;
}
