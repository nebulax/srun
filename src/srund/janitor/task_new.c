
#include <nebase/syslog.h>
#include <nebase/sock/common.h>
#include <nebase/sock/unix.h>

#include <src/srund/shmsg.h>

#include "task.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include <uuid.h>

static int pass_task_to_ancestor(int act_fd, const sr_shmsg_t *sm, int *fds, int fd_num)
{
	int nw = neb_sock_unix_send_with_fds(act_fd, (const char *)sm, sizeof(*sm), fds, fd_num, NULL, 0);
	if (nw == -1) {
		return -1;
	} else if (nw != sizeof(*sm)) {
		neb_syslog(LOG_CRIT, "No all data send out");
		return -1;
	}
	return 0;
}

static void close_recv_fds(int *fds, int fd_num)
{
	for (int i = 0; i < fd_num; i++)
		close(fds[i]);
}

static int is_super_user(uid_t uid)
{
	// TODO add our own admin user
	return (uid == 0);
}

void sr_jnt_task_new(srup_header_t *sh, const struct neb_ucred *cred, int clt_fd, int act_fd)
{
	size_t payload_len = sh->next_len;
	int fd_num = sh->fd_num;
	if (fd_num > SRUN_MSG_MAX_FDS) {
		sr_jnt_task_return_invalid_req(sh, clt_fd);
		return;
	}
	sr_jnt_task_return_continue(sh, clt_fd);

	sr_shmsg_t sm;
	if (is_super_user(cred->uid)) {
		sm.uid = 0;
		sm.gid = 0;
	} else {
		sm.uid = cred->uid;
		sm.gid = cred->gid;
	}
	sm.size = payload_len;
	uuid_generate_time(sm.uuid);

	char *buf = sr_shmsg_create(&sm);
	if (!buf) {
		neb_syslog(LOG_ERR, "Failed to create recv shmsg buffer");
		return;
	}
	int fds[fd_num + 1]; // make sure we have space in fds even if fd_num is 0

	int clt_hup = 0;
	if (!neb_sock_timed_read_ready(clt_fd, SRUP_REQ_PAYLOAD_TIMEOUT, &clt_hup)) {
		if (clt_hup) {
			neb_syslog(LOG_ERR, "Socket hup while reading");
		} else {
			neb_syslog(LOG_ERR, "No data to read after %u ms", SRUP_REQ_PAYLOAD_TIMEOUT);
		}
		goto clean_shmsg;
	}
	if (clt_hup) {
		neb_syslog(LOG_ERR, "Peer fd closed unexpectedly");
		goto clean_shmsg;
	}

	if (fd_num) {
		int recv_fd_num = fd_num;
		int nr = neb_sock_unix_recv_with_fds(clt_fd, buf, payload_len, fds, &recv_fd_num);
		if (nr == -1) {
			neb_syslog(LOG_ERR, "Failed to recv payload and fd from peer");
			sr_jnt_task_return_internal_err(sh, clt_fd);
			goto clean_shmsg;
		} else if ((size_t)nr != payload_len) {
			neb_syslog(LOG_ERR, "Not all payloads read, exp %lu, actual %d", payload_len, nr);
			sr_jnt_task_return_invalid_req(sh, clt_fd);
			close_recv_fds(fds, recv_fd_num);
			goto clean_shmsg;
		} else if (recv_fd_num != fd_num) {
			neb_syslog(LOG_ERR, "Not all fd received, exp %d, actual %d", fd_num, recv_fd_num);
			sr_jnt_task_return_invalid_req(sh, clt_fd);
			close_recv_fds(fds, recv_fd_num);
			goto clean_shmsg;
		}
		fd_num = recv_fd_num;
	} else {
		if (neb_sock_recv_exact(clt_fd, buf, payload_len) != 0) {
			neb_syslog(LOG_ERR, "Failed to recv payload from peer");
			sr_jnt_task_return_internal_err(sh, clt_fd);
			goto clean_shmsg;
		}
	}

	// TODO validate and add new db record

	sr_shmsg_close(&sm, buf);
	uuid_copy(sh->uuid, sm.uuid);
	sr_jnt_task_update_time(sh->uuid);

	if (pass_task_to_ancestor(act_fd, &sm, fds, fd_num) != 0) {
		neb_syslog(LOG_ERR, "Failed to send task to ancestor");
		sr_jnt_task_return_internal_err(sh, clt_fd);
	} else {
		sr_jnt_task_return_ok_added(sh, clt_fd);
	}
	close_recv_fds(fds, fd_num);
	return;

clean_shmsg:
	sr_shmsg_close(&sm, buf);
	sr_shmsg_clean(&sm);
}
