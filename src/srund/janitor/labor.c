
#include "options.h"

#include <nebase/syslog.h>
#include <nebase/sock/common.h>
#include <nebase/sock/unix.h>
#include <srun/protocol.h>

#include "labor.h"
#include "task.h"

#include <unistd.h>

#include <glib.h>

#define SRUP_RECV_TIMEOUT_MSEC 100

static GThreadPool *labor_pool = NULL;
static _Atomic int labor_pool_quiting = 0;

static void labor_exec(gpointer data, gpointer user_data)
{
	int clt_fd = GPOINTER_TO_INT(data);
	int act_fd = GPOINTER_TO_INT(user_data);

	if (labor_pool_quiting)
		goto exit_close;
#if defined(WITH_DEBUG_LOG)
	neb_syslog(LOG_DEBUG, "Handling new peer fd %d", clt_fd);
#endif

	int clt_hup = 0;
	if (!neb_sock_timed_read_ready(clt_fd, SRUP_RECV_TIMEOUT_MSEC, &clt_hup)) {
		if (clt_hup) {
			neb_syslog(LOG_ERR, "Socket hup while reading");
		} else {
			neb_syslog(LOG_ERR, "No data to read after %u ms", SRUP_RECV_TIMEOUT_MSEC);
		}
		goto exit_close;
	}
	if (clt_hup) {
		neb_syslog(LOG_ERR, "Peer fd closed unexpectedly");
		goto exit_close;
	}

	struct neb_ucred cred;
	srup_header_t sh;
	int nr = neb_sock_unix_recv_with_cred(SOCK_SEQPACKET, clt_fd, (char *)&sh, sizeof(sh), &cred);
	if (nr != (int)sizeof(sh)) {
		neb_syslog(LOG_ERR, "Failed to recv srup_header and cred from peer");
		goto exit_close;
	}

	if (sh.magic != SRUP_MAGIC) {
		neb_syslog(LOG_ERR, "Protocol magic not match");
		goto exit_close;
	}
	if (sh.version != SRUP_VERSION) {
		neb_syslog(LOG_ERR, "Protocol version mismatch: peer %u exp %d", sh.version, SRUP_VERSION);
		goto exit_close;
	}

	switch (sh.type) {
	case SRUP_TYPE_REQ_NEW:
		sr_jnt_task_new(&sh, &cred, clt_fd, act_fd);
		break;
	case SRUP_TYPE_REQ_UPDATE:
		sr_jnt_task_set(&sh, &cred, clt_fd);
		break;
	case SRUP_TYPE_REQ_QUERY:
		sr_jnt_task_get(&sh, &cred, clt_fd);
		break;
	case SRUP_TYPE_REQ_TERMINATE:
		sr_jnt_task_kill(&sh, &cred, clt_fd);
		break;
	default:
		neb_syslog(LOG_ERR, "Unsupported message type %u", sh.type);
		break;
	}

exit_close:
#if defined(WITH_DEBUG_LOG)
	neb_syslog(LOG_DEBUG, "Closing peer fd %d", clt_fd);
#endif
	close(clt_fd);
}

int sr_jnt_labor_create(int act_fd)
{
	GError *err;
	gpointer act_p = GINT_TO_POINTER(act_fd);
	labor_pool_quiting = 0;
	labor_pool = g_thread_pool_new(labor_exec, act_p, g_get_num_processors(), TRUE, &err);
	if (!labor_pool) {
		neb_syslog(LOG_ERR, "g_thread_pool_new: %s", err->message);
		g_error_free(err);
		return -1;
	}
	return 0;
}

void sr_jnt_labor_destroy(void)
{
	if (!labor_pool)
		return;
	labor_pool_quiting = 1;
	g_thread_pool_free(labor_pool, FALSE, TRUE);
	labor_pool = NULL;
}

int sr_jnt_labor_handle(int fd)
{
	gpointer p = GINT_TO_POINTER(fd);
	GError *err;
	if (!g_thread_pool_push(labor_pool, p, &err)) {
		neb_syslog(LOG_ERR, "g_thread_pool_push: %s", err->message);
		g_error_free(err);
		return -1;
	}
	return 0;
}
