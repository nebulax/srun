
#include <nebase/syslog.h>

#include "shmsg.h"

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define PREFIX "/srund.shmsg."
#define PREFIX_LEN 13

static _Thread_local char shmsg_path[PREFIX_LEN + UUID_STR_LEN + 1] = PREFIX;

static void sr_shmsg_to_path(sr_shmsg_t *sm)
{
	uuid_unparse_upper(sm->uuid, shmsg_path + PREFIX_LEN);
}

void sr_shmsg_clean(sr_shmsg_t *sm)
{
	sr_shmsg_to_path(sm);
	if (shm_unlink(shmsg_path) == -1)
		neb_syslog(LOG_ERR, "shm_unlink(%s): %m", shmsg_path);
}

char *sr_shmsg_create(sr_shmsg_t *sm)
{
	sr_shmsg_to_path(sm);
	int fd = shm_open(shmsg_path, O_RDWR | O_CREAT | O_EXCL, S_IWUSR | S_IRUSR);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "shm_open(%s): %m", shmsg_path);
		return NULL;
	}
	if (ftruncate(fd, sm->size) == -1) {
		neb_syslog(LOG_ERR, "ftruncate: %m");
		close(fd);
		shm_unlink(shmsg_path);
		return NULL;
	}

	char *addr = mmap(NULL, sm->size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		neb_syslog(LOG_ERR, "mmap(%s): %m", shmsg_path);
		addr = NULL;
	}
	close(fd);
	return addr;
}

char *sr_shmsg_rdopen(sr_shmsg_t *sm)
{
	sr_shmsg_to_path(sm);
	int fd = shm_open(shmsg_path, O_RDONLY, 0);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "shm_open(%s): %m", shmsg_path);
		return NULL;
	}

	char *addr = mmap(NULL, sm->size, PROT_READ, MAP_SHARED, fd, 0);
	if (addr == MAP_FAILED) {
		neb_syslog(LOG_ERR, "mmap(%s): %m", shmsg_path);
		addr = NULL;
	}
	close(fd);
	return addr;
}

const char *sr_shmsg_get_path(void)
{
	return shmsg_path;
}

void sr_shmsg_close(sr_shmsg_t *sm, char *p)
{
	if (munmap(p, sm->size) == -1)
		neb_syslog(LOG_ERR, "munmap: %m");
}
