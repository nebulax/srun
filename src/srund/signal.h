
#ifndef SRUN_SIGNAL_H
#define SRUN_SIGNAL_H 1

#include <nebase/cdefs.h>

extern void sr_signal_block_all(void);

extern void sr_signal_unblock_daemon(void);
extern void sr_signal_unblock_child(void);

#endif
