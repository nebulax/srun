
#ifndef SRUN_MESSAGE_H
#define SRUN_MESSAGE_H 1

#include <nebase/cdefs.h>

#include <uuid.h>

typedef struct {
	uuid_t uuid;
	int size;
	uid_t uid;
	gid_t gid;
} sr_shmsg_t;

extern void sr_shmsg_clean(sr_shmsg_t *sm)
	_nattr_nonnull((1));

extern char *sr_shmsg_create(sr_shmsg_t *sm)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern char *sr_shmsg_rdopen(sr_shmsg_t *sm)
	_nattr_warn_unused_result _nattr_nonnull((1));
extern const char *sr_shmsg_get_path(void);
extern void sr_shmsg_close(sr_shmsg_t *sm, char *p)
	_nattr_nonnull((1, 2));

#endif
