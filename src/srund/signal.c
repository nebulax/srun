
#include <nebase/syslog.h>
#include <nebase/events.h>

#include "signal.h"

#include <signal.h>
#include <pthread.h>

void sr_signal_block_all(void)
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGHUP);

	sigaddset(&set, SIGCHLD);

	ret = pthread_sigmask(SIG_BLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

void sr_signal_unblock_daemon(void)
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);

	sigaddset(&set, SIGCHLD);

	ret = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}

void sr_signal_unblock_child(void)
{
	int ret;
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, SIGTERM);
	sigaddset(&set, SIGINT);
	sigaddset(&set, SIGHUP);

	sigaddset(&set, SIGCHLD);

	ret = pthread_sigmask(SIG_UNBLOCK, &set, NULL);
	if (ret != 0)
		neb_syslog_en(ret, LOG_ERR, "pthread_sigmask: %m");
}
