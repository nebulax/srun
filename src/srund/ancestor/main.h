
#ifndef SRUN_ACT_MAIN_H
#define SRUN_ACT_MAIN_H 1

#include <nebase/cdefs.h>

extern int sr_act_exec(int listen_fd)
	_nattr_warn_unused_result;

#endif
