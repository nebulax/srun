
#include <nebase/syslog.h>
#include <nebase/signal.h>
#include <nebase/sock/unix.h>

#include <srun/message.h>

#include <src/srund/proc.h>
#include <src/srund/shmsg.h>
#include <src/srund/guardian/main.h>
#include <src/srund/guardian/unpack.h>
#include <src/srund/guardian/ctl/io.h>

#include "main.h"
#include "signal.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>
#include <poll.h>
#include <errno.h>

static void shmsg_quit_clean(int fd)
{
	sr_shmsg_t smsg;
	for (;;) {
		ssize_t nr = recv(fd, &smsg, sizeof(smsg), MSG_DONTWAIT);
		if (nr != sizeof(smsg))
			break;
		sr_shmsg_clean(&smsg);
	}
}

static void close_recv_fds(int *fds, int fd_num)
{
	for (int i = 0; i < fd_num; i++) {
		int fd = fds[i];
		if (fd >= 0)
			close(fd);
	}
}

static int go_to_unprivileged(uid_t uid, gid_t gid)
{
	if (setresgid(gid, gid, gid) == -1) {
		neb_syslog(LOG_ERR, "setresgid: %m");
		return -1;
	}
	if (setresuid(uid, uid, uid) == -1) {
		neb_syslog(LOG_ERR, "setresuid: %m");
		return -1;
	}
	return 0;
}

int sr_act_exec(int listen_fd)
{
	int ret = 0;

	if (sr_act_signal_handler_set() != 0) {
		neb_syslog(LOG_ERR, "Failed to set signal handler");
		ret = -1;
		goto exit_return;
	}

	if (sr_proc_sem_child_set_ready() != 0) {
		neb_syslog(LOG_ERR, "Failed to tell we are ready");
		ret = -1;
		goto exit_return;
	}

	struct pollfd pfd = {
		.fd = listen_fd,
		.events = POLLIN,
	};
	for (;;) {
		if (thread_events) {
			if (thread_events & T_E_QUIT) {
				shmsg_quit_clean(listen_fd);
				break;
			}
		}

		if (poll(&pfd, 1, -1) == -1) {
			if (errno == EINTR)
				continue;
			neb_syslog(LOG_ERR, "poll: %m");
			ret = -1;
			break;
		}
		if (pfd.revents & POLLHUP) {
			neb_syslog(LOG_INFO, "peer fd closed, no need to listen");
			break;
		}

		sr_shmsg_t smsg;
		struct iovec iov = {
			.iov_base = &smsg,
			.iov_len = sizeof(smsg),
		};
		char buf[CMSG_SPACE(sizeof(int) * SRUN_MSG_MAX_FDS) + CMSG_SPACE(neb_sock_ucred_cmsg_size)];
		struct msghdr msg = {
			.msg_name = NULL,
			.msg_namelen = 0,
			.msg_iov = &iov,
			.msg_iovlen = 1,
			.msg_control = buf,
			.msg_controllen = sizeof(buf),
		};
		ssize_t nr = recvmsg(listen_fd, &msg, MSG_DONTWAIT);
		if (nr == -1) {
			neb_syslog(LOG_ERR, "recvmsg: %m");
			ret = -1;
			break;
		}
		if (nr != (ssize_t)sizeof(smsg)) {
			neb_syslog(LOG_WARNING, "Invalid msg received: size mismatch");
			continue;
		}

		if (msg.msg_flags & MSG_CTRUNC)
			neb_syslog(LOG_CRIT, "cmsg trunc flag is set");

		int *recv_fd = NULL;
		int fd_num = 0;
		for (struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg); cmsg; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
			if (cmsg->cmsg_level != SOL_SOCKET || cmsg->cmsg_type != SCM_RIGHTS)
				continue;
			recv_fd = (int *)CMSG_DATA(cmsg);
			fd_num = (cmsg->cmsg_len - CMSG_LEN(0)) / sizeof(int);
		}

		pid_t guardian_pid = fork();
		if (guardian_pid == -1) {
			neb_syslog(LOG_ERR, "fork: %m");
			ret = -1;
			sr_shmsg_clean(&smsg);
			thread_events |= T_E_QUIT;
			close_recv_fds(recv_fd, fd_num);
			continue;
		} else if (guardian_pid == 0) {
			/* guardian default to be in the same session and process group with ancestor */
			close(listen_fd);
			listen_fd = -1;

			const char *shmsg_path = sr_shmsg_get_path();
			char *addr = sr_shmsg_rdopen(&smsg);
			if (!addr) {
				neb_syslog(LOG_ERR, "Failed to open shmsg %s:%d", shmsg_path, smsg.size);
				sr_shmsg_clean(&smsg);
				close_recv_fds(recv_fd, fd_num);
				ret = -1;
				break;
			}
			sr_exec_t *se = sr_gud_msg_unpack(addr, smsg.size, recv_fd, fd_num);
			if (!se) {
				neb_syslog(LOG_ERR, "Failed to parse shmsg %s:%d", shmsg_path, smsg.size);
				sr_shmsg_close(&smsg, addr);
				sr_shmsg_clean(&smsg);
				close_recv_fds(recv_fd, fd_num);
				ret = -1;
				break;
			}
			sr_shmsg_close(&smsg, addr);
			sr_shmsg_clean(&smsg);

			int unprivileged = 0;
			if (smsg.uid != 0) {
				unprivileged = 1;
				int home_dirfd = -1;
				if (sr_io_user_chdir(&smsg, &home_dirfd) != 0) {
					neb_syslog(LOG_ERR, "Error occur while chdir to home of uid %u", smsg.uid);
					sr_exec_t_free(se);
					ret = -1;
					break;
				}

				if (home_dirfd >= 0) {
					if (se->dft_index >= 0 && sr_io_user_open_default(&smsg, home_dirfd, se) != 0) {
						neb_syslog(LOG_ERR, "Failed to open default io for uid %u", smsg.uid);
						close(home_dirfd);
						sr_exec_t_free(se);
						ret = -1;
						break;
					}
					close(home_dirfd);
				}
				// TODO close default IO if failed in following steps

				if (go_to_unprivileged(smsg.uid, smsg.gid) != 0) {
					neb_syslog(LOG_ERR, "Failed to change to unprivileged state");
					sr_exec_t_free(se);
					ret = -1;
					break;
				}
			} else {
				if (se->dft_index >= 0 && sr_io_sys_open_default(&smsg, se) != 0) {
					neb_syslog(LOG_ERR, "Failed to open default io");
					sr_exec_t_free(se);
					ret = -1;
					break;
				}
				// TODO close default IO if failed in following steps
			}

			ret = sr_gud_exec(se, unprivileged);
			break;
		} else {
			close_recv_fds(recv_fd, fd_num);
		}
	}

exit_return:
	if (listen_fd >= 0)
		close(listen_fd);
	return ret;
}
