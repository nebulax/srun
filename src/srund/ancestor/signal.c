
#include <nebase/syslog.h>
#include <nebase/events.h>

#include "signal.h"

#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

static void sr_act_sigterm_action(int sig _nattr_unused, siginfo_t *info, void *ucontext _nattr_unused)
{
	if (info->si_uid == 0 || info->si_pid == getppid())
		thread_events |= T_E_QUIT;
}

int sr_act_signal_handler_set(void)
{
	struct sigaction act;

	act.sa_flags = SA_SIGINFO;
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask, SIGHUP);
	act.sa_sigaction = sr_act_sigterm_action;
	if (sigaction(SIGTERM, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	act.sa_flags = SA_RESTART;
	sigemptyset(&act.sa_mask);
	act.sa_handler = SIG_IGN; // use IGN, so need to wait as there will be no zombie
	if (sigaction(SIGCHLD, &act, NULL) == -1) {
		neb_syslog(LOG_ERR, "sigaction: %m");
		return -1;
	}

	return 0;
}
