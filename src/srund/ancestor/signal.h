
#ifndef SRUN_ACT_SIGNAL_H
#define SRUN_ACT_SIGNAL_H 1

#include <nebase/cdefs.h>

extern int sr_act_signal_handler_set(void)
	_nattr_warn_unused_result;

#endif
