
#include <nebase/syslog.h>
#include <nebase/file.h>
#include <nebase/sem.h>

#include "proc.h"

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#define PROC_WAIT_INTERVAL 100000 //100ms
#define PROC_WAIT_TIMES    100    //100, means 10s

static int proc_semid = -1;

enum {
	PROC_SEM_SYNC = 0,
	PROC_SEM_COUNT,
};

int sr_proc_quit_wait(pid_t pid, const char *name)
{
	int term = 1;
	for (int i = 0; i < PROC_WAIT_TIMES; i++) {
		int wstatus;
		pid_t w = waitpid(pid, &wstatus, WNOHANG);
		if (w == -1) {
			neb_syslog(LOG_ERR, "waitpid(%s:%d): %m", name, pid);
			return -1;
		} else if (w == pid) {
			if (WIFEXITED(wstatus)) {
				neb_syslog(LOG_INFO, "Process %s:%d exited with code %d", name, pid, WEXITSTATUS(wstatus));
				return 0;
			}
			if (WIFSIGNALED(wstatus)) {
				neb_syslog(LOG_ERR, "Process %s:%d was terminated by signal %d", name, pid, WTERMSIG(wstatus));
				return -1;
			}
		}

		if (term) { // Only send TERM one time
			term = 0;
			if (kill(pid, SIGTERM) == -1) {
				switch (errno) {
				case ESRCH:
					continue;
					break;
				default:
					neb_syslog(LOG_ERR, "kill(%s:%d): %m", name, pid);
					return -1;
					break;
				}
			}
		}

		usleep(PROC_WAIT_INTERVAL); //100ms
	}

	neb_syslog(LOG_ERR, "Failed to terminate process %s:%d: timeout", name, pid);
	kill(pid, SIGKILL);

	return -1;
}

int sr_proc_sem_create(void)
{
	proc_semid = neb_sem_proc_create(NULL, PROC_SEM_COUNT);
	if (proc_semid < 0) {
		neb_syslog(LOG_ERR, "Failed to create proc sem");
		return -1;
	}

	return 0;
}

void sr_proc_sem_destroy(void)
{
	if (proc_semid >= 0) {
		if (neb_sem_proc_destroy(proc_semid) != 0) {
			neb_syslog(LOG_ERR, "Failed to destroy proc sem");
		} else {
			proc_semid = -1;
		}
	}
}

int sr_proc_sem_child_set_ready(void)
{
	return neb_sem_proc_post(proc_semid, PROC_SEM_SYNC);
}

int sr_proc_sem_wait_child(int num)
{
	struct timespec ts = {.tv_sec = 1};
	return neb_sem_proc_wait_count(proc_semid, PROC_SEM_SYNC, num, &ts);
}

int sr_proc_sem_wait_daemon(void)
{
	struct timespec ts = {.tv_sec = 1};
	return neb_sem_proc_wait_zeroed(proc_semid, PROC_SEM_SYNC, &ts);
}
