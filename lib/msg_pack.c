
#include <nebase/syslog.h>
#include <srun/message.h>

#include <signal.h>

#include <uuid.h>

static int pack_bool_entry(msgpack_packer* mpk, json_t *obj, const char *name, int kid)
{
	msgpack_pack_int8(mpk, kid);
	switch (obj->type) {
	case JSON_TRUE:
		msgpack_pack_true(mpk);
		break;
	case JSON_FALSE:
		msgpack_pack_false(mpk);
		break;
	default:
		neb_syslog(LOG_WARNING, "Value of %s should be boolean", name);
		return -1;
		break;
	}
	return 0;
}

static int pack_integer_entry(msgpack_packer* mpk, json_t *obj, const char *name, int kid)
{
	if (obj->type != JSON_INTEGER) {
		neb_syslog(LOG_WARNING, "Value of %s should be integer", name);
		return -1;
	}
	msgpack_pack_int8(mpk, kid);
	msgpack_pack_int64(mpk, json_integer_value(obj));
	return 0;
}

static int pack_str_entry(msgpack_packer* mpk, json_t *obj, const char *name, int kid)
{
	if (obj->type != JSON_STRING) {
		neb_syslog(LOG_WARNING, "Value of %s should be str", name);
		return -1;
	}
	size_t len = json_string_length(obj);
	if (!len) {
		neb_syslog(LOG_WARNING, "String value size of %s should not be 0", name);
		return -1;
	}
	msgpack_pack_int8(mpk, kid);
	msgpack_pack_str(mpk, len);
	msgpack_pack_str_body(mpk, json_string_value(obj), json_string_length(obj));
	return 0;
}

static int pack_str_array(msgpack_packer* mpk, json_t *obj, const char *name, int kid)
{
	if (obj->type != JSON_ARRAY) {
		neb_syslog(LOG_WARNING, "Value of %s should be array", name);
		return -1;
	}
	size_t array_size = json_array_size(obj);
	if (!array_size) {
		neb_syslog(LOG_WARNING, "No element found in %s", name);
		return -1;
	}
	msgpack_pack_int8(mpk, kid);
	msgpack_pack_array(mpk, array_size);
	for (size_t i = 0; i < array_size; i++) {
		json_t *vobj = json_array_get(obj, i);
		if (vobj->type != JSON_STRING) {
			neb_syslog(LOG_WARNING, "Value of %s/%lu should be str", name, i);
			return -1;
		}
		msgpack_pack_str(mpk, json_string_length(vobj));
		msgpack_pack_str_body(mpk, json_string_value(vobj), json_string_length(vobj));
	}
	return 0;
}

static int check_io_type_conflict(int io_types[SRUN_MSG_MAX_FDS], int nindex, int ntype)
{
	for (int i = 0; i < nindex; i++) {
		int otype = io_types[i];
		if (io_type_is_conflict(ntype, otype)) {
			neb_syslog(LOG_WARNING, "IO Type Conflice: %d/%s - %d/%s", i, io_type_str(otype), nindex, io_type_str(ntype));
			return -1;
		}
	}
	return 0;
}

static int pack_io(msgpack_packer* mpk, json_t *obj, int io_types[SRUN_MSG_MAX_FDS])
{
	if (obj->type != JSON_ARRAY) {
		neb_syslog(LOG_WARNING, "Value of io should be array");
		return -1;
	}
	size_t array_size = json_array_size(obj);
	if (!array_size) {
		neb_syslog(LOG_WARNING, "No element found in io object");
		return -1;
	}
	if (array_size > SRUN_MSG_MAX_FDS) {
		neb_syslog(LOG_WARNING, "Max io number is %u, while we have %lu", SRUN_MSG_MAX_FDS, array_size);
		return -1;
	}
	msgpack_pack_int8(mpk, SRUN_MSG_ROOT_KEY_IO);
	msgpack_pack_array(mpk, array_size);

	for (size_t i = 0; i < array_size; i++) {
		json_t *vobj = json_array_get(obj, i);
		if (vobj->type != JSON_STRING) {
			neb_syslog(LOG_WARNING, "Value of io/%lu should be str", i);
			return -1;
		}
		size_t len = json_string_length(vobj);
		const char *value = json_string_value(vobj);
		int iotype = SRUN_MSG_IO_TYPE_NONE;
		switch (len) {
		case 3:
			if (strcmp(value, "pty") == 0) {
				iotype = SRUN_MSG_IO_TYPE_PTY;
				break;
			} else if (strcmp(value, "dft") == 0) {
				iotype = SRUN_MSG_IO_TYPE_DFT;
				break;
			}
			break;
		case 4:
			if (strcmp(value, "keep") == 0) {
				iotype = SRUN_MSG_IO_TYPE_KEEP;
				break;
			} else if (strcmp(value, "pass") == 0) {
				iotype = SRUN_MSG_IO_TYPE_PASS;
				break;
			}
			break;
		case 5:
			if (strcmp(value, "stdin") == 0) {
				iotype = SRUN_MSG_IO_TYPE_STDIN;
				break;
			}
			break;
		case 6:
			if (strcmp(value, "stdout") == 0) {
				iotype = SRUN_MSG_IO_TYPE_STDOUT;
				break;
			} else if (strcmp(value, "stderr") == 0) {
				iotype = SRUN_MSG_IO_TYPE_STDERR;
				break;
			}
			break;
		default:
			break;
		}
		if (iotype == SRUN_MSG_IO_TYPE_NONE) {
			neb_syslog(LOG_WARNING, "Unsupported io type value %s", value);
			return -1;
		}
		if (check_io_type_conflict(io_types, i, iotype) != 0)
			return -1;
		msgpack_pack_int(mpk, iotype);
		io_types[i] = iotype;
	}
	return 0;
}

static int pack_wait(msgpack_packer* mpk, json_t *obj, int wait_ios[SRUN_MSG_MAX_FDS])
{
	if (obj->type != JSON_OBJECT) {
		neb_syslog(LOG_WARNING, "Value of wait should be object");
		return -1;
	}
	size_t map_size = json_object_size(obj);
	if (!map_size) {
		neb_syslog(LOG_WARNING, "No kv pairs in wait object");
		return -1;
	}
	if (map_size > 2) {
		neb_syslog(LOG_WARNING, "There should be at most 2 objects in wait object");
		return -1;
	}
	msgpack_pack_int8(mpk, SRUN_MSG_ROOT_KEY_WAIT);
	msgpack_pack_map(mpk, map_size);

	int type_seen = 0;
	const char *key;
	json_t *vobj;
	json_object_foreach(obj, key, vobj) {
		if (type_seen && strcmp(key, "timeout")) {
			neb_syslog(LOG_WARNING, "There should be only 1 wait type");
			return -1;
		}
		int added = 0;
		switch (key[0]) {
		case 'f':
			if (strcmp(key, "fdin") == 0) {
				if (vobj->type != JSON_INTEGER) {
					neb_syslog(LOG_WARNING, "Value of wait/fdin should be integer");
					return -1;
				}
				int64_t index = json_integer_value(vobj);
				if (index < 0 || index >= SRUN_MSG_MAX_FDS) {
					neb_syslog(LOG_WARNING, "Value of wait/fdin (%ld) out of range (0-%u)", index, SRUN_MSG_MAX_FDS);
					return -1;
				}
				msgpack_pack_int8(mpk, SRUN_MSG_WAIT_KEY_FDIN);
				msgpack_pack_int64(mpk, index);
				wait_ios[index] = SRUN_MSG_WAIT_KEY_FDIN;
				added = 1;
				type_seen = 1;
				break;
			} else if (strcmp(key, "fdhup") == 0) {
				if (vobj->type != JSON_INTEGER) {
					neb_syslog(LOG_WARNING, "Value of wait/fdhup should be integer");
					return -1;
				}
				int64_t index = json_integer_value(vobj);
				if (index < 0 || index >= SRUN_MSG_MAX_FDS) {
					neb_syslog(LOG_WARNING, "Value of wait/fdout (%ld) out of range(0-%u)", index, SRUN_MSG_MAX_FDS);
					return -1;
				}
				msgpack_pack_int8(mpk, SRUN_MSG_WAIT_KEY_FDHUP);
				msgpack_pack_int64(mpk, index);
				wait_ios[index] = SRUN_MSG_WAIT_KEY_FDHUP;
				added = 1;
				type_seen = 1;
				break;
			}
			break;
		case 's':
			if (strcmp(key, "signal") == 0) {
				if (vobj->type != JSON_INTEGER) {
					neb_syslog(LOG_WARNING, "Value of wait/signal should be integer");
					return -1;
				}
				int64_t signo = json_integer_value(vobj);
				if (signo < SRUN_SIGNO_MIN || signo > SRUN_SIGNO_MAX) {
					neb_syslog(LOG_WARNING, "Value of wait/signal (%ld) out of range (%u-%u)", signo, SRUN_SIGNO_MIN, SRUN_SIGNO_MAX);
					return -1;
				}
				msgpack_pack_int8(mpk, SRUN_MSG_WAIT_KEY_SIGNAL);
				msgpack_pack_int64(mpk, signo);
				added = 1;
				type_seen = 1;
				break;
			}
			break;
		case 't':
			if (strcmp(key, "timeout") == 0) {
				if (pack_integer_entry(mpk, vobj, "wait/timeout", SRUN_MSG_WAIT_KEY_TIMEOUT) != 0)
					return -1;
				added = 1;
			}
			break;
		default:
			break;
		}
		if (!added) {
			neb_syslog(LOG_WARNING, "Unsupported wait key %s", key);
			return -1;
		}
	}

	return 0;
}

static int pack_concurrency(msgpack_packer* mpk, json_t *obj)
{
	if (obj->type != JSON_OBJECT) {
		neb_syslog(LOG_WARNING, "Value of concurrency should be object");
		return -1;
	}
	size_t map_size = json_object_size(obj);
	if (!map_size) {
		neb_syslog(LOG_WARNING, "No kv pairs in concurrency object");
		return -1;
	}
	msgpack_pack_int8(mpk, SRUN_MSG_ROOT_KEY_CONCURRENCY);
	msgpack_pack_map(mpk, map_size);

	const char *key;
	json_t *vobj;
	json_object_foreach(obj, key, vobj) {
		int added = 0;
		switch (key[0]) {
		case 'i':
			if (strcmp(key, "id") == 0) {
				if (vobj->type != JSON_STRING) {
					neb_syslog(LOG_WARNING, "Value of concurrency/id should be uuid string");
					return -1;
				}
				size_t len = json_string_length(vobj);
				if (len < UUID_STR_LEN - 1) {
					neb_syslog(LOG_WARNING, "Size of concurrency/id %lu is too small", len);
					return -1;
				}
				uuid_t uuid;
				if (uuid_parse(json_string_value(vobj), uuid) != 0) {
					neb_syslog(LOG_WARNING, "Value of concurrency/id is not valid uuid");
					return -1;
				}
				msgpack_pack_int8(mpk, SRUN_MSG_CONCURRENCY_KEY_ID);
				msgpack_pack_bin(mpk, sizeof(uuid));
				msgpack_pack_bin_body(mpk, &uuid, sizeof(uuid));
				added = 1;
				break;
			}
			break;
		case 'n':
			if (strcmp(key, "num") == 0) {
				if (pack_integer_entry(mpk, vobj, "concurrency/num", SRUN_MSG_CONCURRENCY_KEY_NUM) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 't':
			if (strcmp(key, "timeout") == 0) {
				if (pack_integer_entry(mpk, vobj, "concurrency/timeout", SRUN_MSG_CONCURRENCY_KEY_TIMEOUT) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		default:
			break;
		}
		if (!added) {
			neb_syslog(LOG_WARNING, "Unsupported concurrency key %s", key);
			return -1;
		}
	}

	return 0;
}

static int pack_cred(msgpack_packer* mpk, json_t *obj)
{
	if (obj->type != JSON_OBJECT) {
		neb_syslog(LOG_WARNING, "Value of cred should be object");
		return -1;
	}
	size_t map_size = json_object_size(obj);
	if (!map_size) {
		neb_syslog(LOG_WARNING, "No kv pairs in cred object");
		return -1;
	}
	msgpack_pack_int8(mpk, SRUN_MSG_ROOT_KEY_CRED);
	msgpack_pack_map(mpk, map_size);

	const char *key;
	json_t *vobj;
	json_object_foreach(obj, key, vobj) {
		int added = 0;
		switch (key[0]) {
		case 'g': // TODO use group name
			if (strcmp(key, "gid") == 0) {
				if (pack_str_entry(mpk, vobj, "cred/gid", SRUN_MSG_CRED_KEY_GID) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'u': // TODO use user name
			if (strcmp(key, "uid") == 0) {
				if (pack_str_entry(mpk, vobj, "cred/uid", SRUN_MSG_CRED_KEY_UID) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		default:
			break;
		}
		if (!added) {
			neb_syslog(LOG_WARNING, "Unsupported cred key %s", key);
			return -1;
		}
	}

	return 0;
}

static int verify_wait_io(int wait_ios[SRUN_MSG_MAX_FDS], int io_types[SRUN_MSG_MAX_FDS])
{
	for (int i = 0; i < SRUN_MSG_MAX_FDS; i++) {
		int wait_type = wait_ios[i];
		int io_type = io_types[i];
		switch (wait_type) {
		case SRUN_MSG_WAIT_KEY_FDIN:
			switch (io_type) {
			case SRUN_MSG_IO_TYPE_STDIN:
			case SRUN_MSG_IO_TYPE_PASS:
				break;
			default:
				neb_syslog(LOG_WARNING, "io %d: wait type is fdhup, while io type is %s", i, io_type_str(io_type));
				return -1;
				break;
			}
			break;
		case SRUN_MSG_WAIT_KEY_FDHUP:
			if (io_type != SRUN_MSG_IO_TYPE_PASS) {
				neb_syslog(LOG_WARNING, "io %d: wait type is fdhup, while io type is %s", i, io_type_str(io_type));
				return -1;
			}
			break;
		default:
			break;
		}
	}
	return 0;
}

int srun_msg_pack_from_json(msgpack_packer* mpk, json_t *jobj)
{
	if (jobj->type != JSON_OBJECT) {
		neb_syslog(LOG_WARNING, "Root should be an object");
		return -1;
	}

	size_t map_size = json_object_size(jobj);
	if (!map_size) {
		neb_syslog(LOG_WARNING, "No kv pairs in root object");
		return -1;
	}

	msgpack_pack_map(mpk, map_size);

	int io_types[SRUN_MSG_MAX_FDS] = {};
	int wait_ios[SRUN_MSG_MAX_FDS] = {};

	const char *key;
	json_t *vobj;
	json_object_foreach(jobj, key, vobj) {
		int added = 0;
		switch (key[0]) {
		case 'a':
			if (strcmp(key, "argv") == 0) {
				if (pack_str_array(mpk, vobj, "argv", SRUN_MSG_ROOT_KEY_ARGV) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'c':
			if (strcmp(key, "cred") == 0) {
				if (pack_cred(mpk, vobj) != 0)
					return -1;
				added = 1;
				break;
			}
			if (strcmp(key, "concurrency") == 0) {
				if (pack_concurrency(mpk, vobj) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'e':
			if (strcmp(key, "envp") == 0) {
				if (pack_str_array(mpk, vobj, "envp", SRUN_MSG_ROOT_KEY_ENVP) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'f':
			if (strcmp(key, "filename") == 0) {
				if (pack_str_entry(mpk, vobj, "filename", SRUN_MSG_ROOT_KEY_FILENAME) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'i':
			if (strcmp(key, "io") == 0) {
				if (pack_io(mpk, vobj, io_types) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 'o':
			if (strcmp(key, "orphan") == 0) {
				if (pack_bool_entry(mpk, vobj, "orphan", SRUN_MSG_ROOT_KEY_ORPHAN) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		case 's':
			if (strcmp(key, "sched") == 0) {
				;//TODO
				added = 1;
				break;
			}
			break;
		case 'w':
			if (strcmp(key, "wait") == 0) {
				if (pack_wait(mpk, vobj, wait_ios) != 0)
					return -1;
				added = 1;
				break;
			}
			break;
		default:
			break;
		}
		if (!added) {
			neb_syslog(LOG_WARNING, "Unsupport root key %s", key);
			return -1;
		}
	}

	// post check following

	if (verify_wait_io(wait_ios, io_types) != 0)
		return -1;

	return 0;
}
