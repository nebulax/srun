
#include <nebase/syslog.h>
#include <srun/message.h>

const char *io_type_str(int io_type)
{
	switch (io_type) {
	case SRUN_MSG_IO_TYPE_NONE:
		return "none";
	case SRUN_MSG_IO_TYPE_DFT:
		return "dft";
	case SRUN_MSG_IO_TYPE_KEEP:
		return "keep";
	case SRUN_MSG_IO_TYPE_PASS:
		return "pass";
	case SRUN_MSG_IO_TYPE_PTY:
		return "pty";
	case SRUN_MSG_IO_TYPE_STDERR:
		return "stderr";
	case SRUN_MSG_IO_TYPE_STDIN:
		return "stdin";
	case SRUN_MSG_IO_TYPE_STDOUT:
		return "stdout";
	default:
		return "unsupported";
	}
}

int io_type_is_conflict(int ntype, int otype)
{
	int conflict = 0;
	switch (ntype) {
	case SRUN_MSG_IO_TYPE_DFT:
		switch (otype) {
		case SRUN_MSG_IO_TYPE_DFT:
		case SRUN_MSG_IO_TYPE_PTY:
		case SRUN_MSG_IO_TYPE_STDERR:
		case SRUN_MSG_IO_TYPE_STDOUT:
			conflict = 1;
			break;
		default:
			break;
		}
		break;
	case SRUN_MSG_IO_TYPE_PTY:
		switch (otype) {
		case SRUN_MSG_IO_TYPE_PTY:
		case SRUN_MSG_IO_TYPE_DFT:
		case SRUN_MSG_IO_TYPE_STDERR:
		case SRUN_MSG_IO_TYPE_STDIN:
		case SRUN_MSG_IO_TYPE_STDOUT:
			conflict = 1;
			break;
		default:
			break;
		}
		break;
	case SRUN_MSG_IO_TYPE_STDERR:
		switch (otype) {
		case SRUN_MSG_IO_TYPE_PTY:
		case SRUN_MSG_IO_TYPE_DFT:
		case SRUN_MSG_IO_TYPE_STDERR:
			conflict = 1;
			break;
		default:
			break;
		}
		break;
	case SRUN_MSG_IO_TYPE_STDIN:
		switch (otype) {
		case SRUN_MSG_IO_TYPE_PTY:
		case SRUN_MSG_IO_TYPE_STDIN:
			conflict = 1;
			break;
		default:
			break;
		}
		break;
	case SRUN_MSG_IO_TYPE_STDOUT:
		switch (otype) {
		case SRUN_MSG_IO_TYPE_PTY:
		case SRUN_MSG_IO_TYPE_DFT:
		case SRUN_MSG_IO_TYPE_STDOUT:
			conflict = 1;
			break;
		default:
			break;
		}
		break;
	default:
		break;
	}
	return conflict;
}
