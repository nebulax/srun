
#ifndef SRUN_LIB_SCHED_H
#define SRUN_LIB_SCHED_H 1

#include <stdbool.h>

extern int srun_sched_policy_default;
extern int srun_sched_priority_default;

extern bool srun_sched_policy_is_valid(int policy);
extern bool srun_sched_priority_is_valid(int policy, int priority);

#endif
