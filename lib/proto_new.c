
#include <nebase/syslog.h>
#include <nebase/sock/common.h>
#include <nebase/sock/unix.h>

#include <srun/protocol.h>

#include <unistd.h>

static int send_header(int fd, int payload_len, int fd_num)
{
	struct srup_header srup = {
		.magic = SRUP_MAGIC,
		.version = SRUP_VERSION,
		.type = SRUP_TYPE_REQ_NEW,
		.next_len = payload_len,
		.fd_num = fd_num,
	};

	int nw = neb_sock_unix_send_with_cred(fd, (const char *)&srup, sizeof(srup), NULL, 0);
	return (nw != sizeof(srup)) ? -1 : 0;
}

static int recv_continue(int fd)
{
	int hup = 0;
	if (!neb_sock_timed_read_ready(fd, SRUND_WAIT_TIMEOUT, &hup)) {
		if (hup) {
			neb_syslog(LOG_ERR, "Socket hup while reading");
		} else {
			neb_syslog(LOG_ERR, "No data to read after %u ms", SRUND_WAIT_TIMEOUT);
		}
		return -1;
	}
	if (hup) {
		neb_syslog(LOG_ERR, "Socket has been closed unexpectedly");
		return -1;
	}
	struct srup_header srup = {};
	if (neb_sock_recv_exact(fd, &srup, sizeof(srup)) != 0)
		return -1;
	switch (srup.type) {
	case SRUP_TYPE_RSP_CONTINUE:
		return 0;
		break;
	case SRUP_TYPE_RSP_ERR:
		neb_syslog(LOG_ERR, "Error rsp received: %s", srup_errmsg(srup.err_code));
		return -1;
		break;
	default:
		neb_syslog(LOG_ERR, "Invalid type in rsp header: %d", srup.type);
		return -1;
		break;
	}
}

static int recv_uuid(int fd, uuid_t uuid)
{
	int hup = 0;
	if (!neb_sock_timed_read_ready(fd, SRUND_WAIT_TIMEOUT, &hup)) {
		if (hup) {
			neb_syslog(LOG_ERR, "Socket hup while reading");
		} else {
			neb_syslog(LOG_ERR, "No data to read after %u ms", SRUND_WAIT_TIMEOUT);
		}
		return -1;
	}
	struct srup_header srup = {};
	if (neb_sock_recv_exact(fd, &srup, sizeof(srup)) != 0) {
		neb_syslog(LOG_ERR, "Failed to recv final rsp from server");
		if (hup)
			neb_syslog(LOG_ERR, "And server has closed the socket");
		return -1;
	}
	switch (srup.type) {
	case SRUP_TYPE_RSP_OK:
		uuid_copy(uuid, srup.uuid);
		return 0;
		break;
	case SRUP_TYPE_RSP_ERR:
		neb_syslog(LOG_ERR, "Error rsp received: %s", srup_errmsg(srup.err_code));
		return -1;
		break;
	default:
		neb_syslog(LOG_ERR, "Invalid type in rsp header: %d", srup.type);
		return -1;
		break;
	}
}

int srun_proto_new(const char *msg, int len, int *fdp, int fd_num, uuid_t uuid)
{
	int fd = neb_sock_unix_new_connected(SOCK_SEQPACKET, srund_addr, SRUND_CONNECT_TIMEOUT);
	if (fd == -1) {
		neb_syslog(LOG_ERR, "Failed to connect to %s", srund_addr);
		return -1;
	}

	if (send_header(fd, len, fd_num) != 0) {
		neb_syslog(LOG_ERR, "Failed to send header to %s", srund_addr);
		close(fd);
		return -1;
	}

	if (recv_continue(fd) != 0) {
		neb_syslog(LOG_ERR, "Failed to recv continue rsp from %s", srund_addr);
		close(fd);
		return -1;
	}

	if (fd_num) {
		int nw = neb_sock_unix_send_with_fds(fd, msg, len, fdp, fd_num, NULL, 0);
		if (nw != len) {
			neb_syslog(LOG_ERR, "Failed to send payload to %s", srund_addr);
			close(fd);
			return -1;
		}
	} else {
		if (neb_sock_send_exact(fd, msg, len) != 0) {
			neb_syslog(LOG_ERR, "Failed to send payload to %s", srund_addr);
			close(fd);
			return -1;
		}
	}

	if (recv_uuid(fd, uuid) != 0) {
		neb_syslog(LOG_ERR, "Failed to recv final rsp from %s", srund_addr);
		close(fd);
		return -1;
	}

	close(fd);
	return 0;
}
