
#include "options.h"

#include <srun/protocol.h>

const char *srund_addr = SRUND_LISTEN_SOCKET;

const char *srup_errmsg(int errcode)
{
	switch (errcode) {
	case SRUP_ERR_INVALID:
		return "Invalid request";
		break;
	case SRUP_ERR_DENIED:
		return "Access denied";
		break;
	case SRUP_ERR_NOTFOUND:
		return "No record found";
		break;
	case SRUP_ERR_INTERNAL:
		return "Internal error";
		break;
	default:
		return "Unsupported error code";
		break;
	}
}
